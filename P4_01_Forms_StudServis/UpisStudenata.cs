﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Collections;
using System.Diagnostics;


namespace _P4_01_Forms_StudServis
{
    public partial class UpisStudenata : Form
    {
        ArrayList predmetilist = new ArrayList 
        {
            new ArrayList {"Uvod u programiranje","Matematicka analiza 1", "Matematicka analiza 2", "Uvod u web programiranje", "Uvod u OO programiranje"},
            new ArrayList {"Strukture podataka i algoritmi", "Dizajn i analiza algoritama", "Interaktivno programiranje"},
            new ArrayList {"Uvod u baze podataka", "Racunarske mreze", "Uvod u numericku analizu", "Simbolicka izracunavanja"},
            new ArrayList {"Teorija programskih jezika", "Inteligentni sistemi", "Razvoj softvera"},
            new ArrayList {"Teorija informacija i kodiranje", "Web programiranje"}
        };

        ArrayList godine = new ArrayList
        {
            "1. godina", "2. godina", "3. godina", "1. godina (master)", "2. godina (master)"
        };

        
        public UpisStudenata()
        {
            InitializeComponent();
            
            // DataSource je property ComboBox kontrole koja oznacava izvor odakle se pribavljaju vrednosti
            godina.DataSource = godine;
           

            // Indeks vrednosti koja je odabrana. Brojanje pocinje od 0
            godina.SelectedIndex = 0;
            PredmetiUpdate();
        }

        // Updatejuje CheckedListBox predmetima iz odgovarajuce godine
        private void PredmetiUpdate()
        {
            predmeti.Items.Clear();
            
            ArrayList predmetiugod = predmetilist[godina.SelectedIndex] as ArrayList;

            foreach (string predmet in predmetiugod)
            {
                // Property Items pamti listu elemenata CheckedListBox-a
                predmeti.Items.Add(predmet);
            }
        }

        private string NapraviPoruku()
        {
            StringBuilder sb = new StringBuilder();
         
            sb.AppendFormat("Ime i prezime: {0} {1}\n", ime.Text, prezime.Text);
            sb.AppendFormat("Godina koja se upisuje: {0}\n", godina.Text);

            sb.Append("Nacin finansiranja: ");
            if (izBudzeta.Checked)                  // Property Checked oznacava da li je RadioButton oznacen ili nije
                sb.AppendLine(izBudzeta.Text);
            else
                sb.AppendLine(samofinansiranje.Text);
            
            string dane;
            if (uIstuGod.Checked)                   // Da li je CheckBox oznacen
                dane = "Da";
            else
                dane = "Ne";
            sb.AppendFormat("Da li se student ponovo upisuje: {0}\n", dane);

            sb.AppendFormat("Datum upisa: {0}\n", datumUpisa.Text);
            sb.AppendLine("Predmeti koje student slusa: ");

            foreach (string predmet in predmeti.CheckedItems)
            {
                sb.AppendLine("\t" + predmet);
            }
            sb.AppendLine();

            return sb.ToString();
        }

        private void Resetuj()
        {
            ime.Clear();
            prezime.Clear();

            godina.SelectedIndex = 0;
            datumUpisa.Value = DateTime.Today;

            PredmetiUpdate();        
            
            uIstuGod.Checked = false;
        }

        // Metoda dodeljena eventu Click dugmeta dodaj. Event se podize kada kliknemo na dugme. Podesava se u prozoru Properties, kartici Events
        private void dodaj_Click(object sender, EventArgs e)
        {
            MessageBox.Show(NapraviPoruku(), "Poruka",MessageBoxButtons.OK, MessageBoxIcon.Information);
            Debug.WriteLine("Poruka generisana!");
        }

        // Event koji se poziva kada promenimo vrednost ComboBox-a, tj. kada odaberemo godinu
        private void godina_SelectedIndexChanged(object sender, EventArgs e)
        {
            PredmetiUpdate();
        }
        
        private void obrisi_Click(object sender, EventArgs e)
        {
            Resetuj();
        }

        // Event koji se podize kada se forma ucita (prikaze)
        private void UpisStudenata_Load(object sender, EventArgs e)
        {
            // naknadno dodavanje funkcije u event dodaj.Click. Definisana pomocu Lambda izraza i poziva se nakon dodaj_Click
            dodaj.Click += (x, y) => MessageBox.Show("Done!");
        }

        // Event koji se podize kada korisnik pritisne taster na tastaturi
        // parametar sender oznacava koja komponenta je pozvala funkciju (u ovom slucaju znamo da je TextBox (cak i koji), ali 
        // moze da se desi da je fja povezana na vise komponenti) 
        // parametar e daje dodatne parametre, konkretno, koje dugme je pritisnuto
        private void ime_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox senderText = sender as TextBox;
            
            if (e.KeyChar == (char) Keys.Enter)
                MessageBox.Show("Pritisnut je enter na komponenti " + senderText.Name);
        }

        private void ime_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
