﻿namespace _P4_01_Forms_StudServis
{
    partial class UpisStudenata
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIme = new System.Windows.Forms.Label();
            this.ime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.godina = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.samofinansiranje = new System.Windows.Forms.RadioButton();
            this.izBudzeta = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.datumUpisa = new System.Windows.Forms.DateTimePicker();
            this.uIstuGod = new System.Windows.Forms.CheckBox();
            this.dodaj = new System.Windows.Forms.Button();
            this.obrisi = new System.Windows.Forms.Button();
            this.prezime = new System.Windows.Forms.TextBox();
            this.predmeti = new System.Windows.Forms.CheckedListBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblIme
            // 
            this.lblIme.AutoSize = true;
            this.lblIme.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIme.Location = new System.Drawing.Point(102, 50);
            this.lblIme.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblIme.Name = "lblIme";
            this.lblIme.Size = new System.Drawing.Size(59, 33);
            this.lblIme.TabIndex = 0;
            this.lblIme.Text = "Ime";
            // 
            // ime
            // 
            this.ime.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ime.Location = new System.Drawing.Point(176, 44);
            this.ime.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ime.Name = "ime";
            this.ime.Size = new System.Drawing.Size(238, 41);
            this.ime.TabIndex = 1;
            this.ime.TextChanged += new System.EventHandler(this.ime_TextChanged);
            this.ime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ime_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(506, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 33);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(72, 131);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 33);
            this.label3.TabIndex = 4;
            this.label3.Text = "Godina";
            // 
            // godina
            // 
            this.godina.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.godina.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.godina.FormattingEnabled = true;
            this.godina.Items.AddRange(new object[] {
            "jsdiaos",
            "dakjsiojd",
            "fajsdiofasd"});
            this.godina.Location = new System.Drawing.Point(176, 125);
            this.godina.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.godina.Name = "godina";
            this.godina.Size = new System.Drawing.Size(238, 41);
            this.godina.TabIndex = 3;
            this.godina.SelectedIndexChanged += new System.EventHandler(this.godina_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.samofinansiranje);
            this.groupBox1.Controls.Add(this.izBudzeta);
            this.groupBox1.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(18, 215);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Size = new System.Drawing.Size(400, 192);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Način finansiranja";
            // 
            // samofinansiranje
            // 
            this.samofinansiranje.AutoSize = true;
            this.samofinansiranje.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.samofinansiranje.Location = new System.Drawing.Point(12, 81);
            this.samofinansiranje.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.samofinansiranje.Name = "samofinansiranje";
            this.samofinansiranje.Size = new System.Drawing.Size(241, 37);
            this.samofinansiranje.TabIndex = 1;
            this.samofinansiranje.Text = "Samofinansiranje";
            this.samofinansiranje.UseVisualStyleBackColor = true;
            // 
            // izBudzeta
            // 
            this.izBudzeta.AutoSize = true;
            this.izBudzeta.Checked = true;
            this.izBudzeta.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.izBudzeta.Location = new System.Drawing.Point(12, 37);
            this.izBudzeta.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.izBudzeta.Name = "izBudzeta";
            this.izBudzeta.Size = new System.Drawing.Size(167, 37);
            this.izBudzeta.TabIndex = 0;
            this.izBudzeta.TabStop = true;
            this.izBudzeta.Text = "Iz budzeta";
            this.izBudzeta.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 446);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 33);
            this.label4.TabIndex = 7;
            this.label4.Text = "Datum upisa";
            // 
            // datumUpisa
            // 
            this.datumUpisa.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datumUpisa.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datumUpisa.Location = new System.Drawing.Point(176, 440);
            this.datumUpisa.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.datumUpisa.Name = "datumUpisa";
            this.datumUpisa.Size = new System.Drawing.Size(238, 41);
            this.datumUpisa.TabIndex = 7;
            // 
            // uIstuGod
            // 
            this.uIstuGod.AutoSize = true;
            this.uIstuGod.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uIstuGod.Location = new System.Drawing.Point(512, 133);
            this.uIstuGod.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.uIstuGod.Name = "uIstuGod";
            this.uIstuGod.Size = new System.Drawing.Size(270, 37);
            this.uIstuGod.TabIndex = 4;
            this.uIstuGod.Text = "Ponovo u istu godinu";
            this.uIstuGod.UseVisualStyleBackColor = true;
            // 
            // dodaj
            // 
            this.dodaj.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dodaj.Location = new System.Drawing.Point(176, 556);
            this.dodaj.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.dodaj.Name = "dodaj";
            this.dodaj.Size = new System.Drawing.Size(146, 52);
            this.dodaj.TabIndex = 8;
            this.dodaj.Text = "Dodaj";
            this.dodaj.UseVisualStyleBackColor = true;
            this.dodaj.Click += new System.EventHandler(this.dodaj_Click);
            // 
            // obrisi
            // 
            this.obrisi.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.obrisi.Location = new System.Drawing.Point(618, 556);
            this.obrisi.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.obrisi.Name = "obrisi";
            this.obrisi.Size = new System.Drawing.Size(150, 52);
            this.obrisi.TabIndex = 9;
            this.obrisi.Text = "Obrisi";
            this.obrisi.UseVisualStyleBackColor = true;
            this.obrisi.Click += new System.EventHandler(this.obrisi_Click);
            // 
            // prezime
            // 
            this.prezime.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prezime.Location = new System.Drawing.Point(618, 44);
            this.prezime.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.prezime.Name = "prezime";
            this.prezime.Size = new System.Drawing.Size(238, 41);
            this.prezime.TabIndex = 2;
            this.prezime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ime_KeyPress);
            // 
            // predmeti
            // 
            this.predmeti.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.predmeti.CheckOnClick = true;
            this.predmeti.Font = new System.Drawing.Font("Comic Sans MS", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.predmeti.FormattingEnabled = true;
            this.predmeti.Items.AddRange(new object[] {
            "a",
            "v",
            "s",
            "a",
            "e"});
            this.predmeti.Location = new System.Drawing.Point(512, 215);
            this.predmeti.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.predmeti.Name = "predmeti";
            this.predmeti.Size = new System.Drawing.Size(344, 256);
            this.predmeti.TabIndex = 6;
            // 
            // UpisStudenata
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 652);
            this.Controls.Add(this.predmeti);
            this.Controls.Add(this.prezime);
            this.Controls.Add(this.obrisi);
            this.Controls.Add(this.dodaj);
            this.Controls.Add(this.uIstuGod);
            this.Controls.Add(this.datumUpisa);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.godina);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ime);
            this.Controls.Add(this.lblIme);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.MaximizeBox = false;
            this.Name = "UpisStudenata";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Upis studenata";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.UpisStudenata_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIme;
        private System.Windows.Forms.TextBox ime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox godina;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton samofinansiranje;
        private System.Windows.Forms.RadioButton izBudzeta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker datumUpisa;
        private System.Windows.Forms.CheckBox uIstuGod;
        private System.Windows.Forms.Button dodaj;
        private System.Windows.Forms.Button obrisi;
        private System.Windows.Forms.TextBox prezime;
        private System.Windows.Forms.CheckedListBox predmeti;
    }
}

