﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _P1_03_property_indexer
{
    class Program
    {
        static void Main(string[] args)
        {
            // properties
            Properties();

           
            // indekseri
            Indexer();
        }

        private static void Properties()
        {
            ScreenPosition origin = new ScreenPosition(0, 0);
            origin.X = 10;  // poziv set metode
            origin.X += 10; // poziv get i set metode
            // origin.SetX(origin.GetX()+10);
            origin.Y = 20;

            try
            {
                origin.X = 1000000;
            }
            catch (ArgumentOutOfRangeException ae)
            {
                Console.WriteLine(ae.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            origin.Caption = "Koordinatni pocetak";


            BankAccount ba = new BankAccount();
            
            ba.Deposit(1000);
            Console.WriteLine($"Balance after deposit: {ba.Balance}");  // Balance je read-only property
            ba.Withdraw(50);
            Console.WriteLine($"Balance after withdrawal: {ba.Balance}");


            // property moze da se koristi za inicijalizaciju objekta
            Triangle t = new Triangle() 
            { 
                Side1Length = 1, 
                Side2Length = 2 
            }; 

            Triangle t1 = new Triangle(side1length: 1, side3length: 3);
        }

        private static void Indexer()
        {
            IntBits ib = new IntBits();
            
            ib[1] = true;               // podesi prvi bit na true!!
            ib[2] = false;
            Console.WriteLine(ib[1]);
        }
    }
}
