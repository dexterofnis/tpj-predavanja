﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_03_property_indexer
{

    class BankAccount
    {
        int bal = 0;

        public int Balance
        {
            get { return bal; }     // stanje na racunu je read-only za korisnika, ali klasa moze da ga po potrebi menja
            private set             
            {
                bal = value;
            }
        }

        public void Deposit(int amount)
        {
            if (amount > 0) 
                Balance += amount;
        }

        public bool Withdraw(int amount)
        {
            if ((amount <= Balance) && (amount > 0))
            {
                Balance -= amount;
                return true;
            }
            else
                return false;
        }

    } 
}
