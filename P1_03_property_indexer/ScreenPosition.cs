﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_03_property_indexer
{
    struct ScreenPosition 
    {
        private int _x, _y;


        public ScreenPosition(int X, int Y)
        {
            Caption = "Noname";
            this._x = rangeCheckedX(X);
            this._y = rangeCheckedY(Y);
        }
        
        public int X
        {
            get
            { 
                return this._x; 
            }
            set
            {
                this._x = rangeCheckedX(value); // value je vrednost koja je dodeljena
            }
        }
        
        public int Y
        {
            get 
            {
                return this._y; 
            }
            set 
            { 
                this._y = rangeCheckedY(value); 
            }
        }

        private static int rangeCheckedX(int x)
        {
            if (x < 0 || x > 1280)
            {
                throw new ArgumentOutOfRangeException("X");
            }
            return x;
        }

        private static int rangeCheckedY(int y)
        {
            if (y < 0 || y > 1024)
            {
                throw new ArgumentOutOfRangeException("Y");
            }
            return y;
        }

        public override string ToString()
        {
            return String.Format("Koordinate ta\v cke {0} su: {1} i {2}", "Caption", _x, _y);
        }

        // ukoliko get i set ne rade nista drugo osim dodele vrednosti odgovarajucem fieldu
        // promenljivu nema potrebe eksplicitno deklarisati

        public string Caption       
        { 
            get; set; 
        }       

    }
}
