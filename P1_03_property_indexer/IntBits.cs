﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_03_property_indexer
{

    struct IntBits
    {
        private int bits;

        public IntBits(int initialBitValue)
        {
            bits = initialBitValue;
        }

        
        // indekser
        public bool this[int index]
        {
            get
            {
                return (bits & (1 << index)) != 0;
            }

            set
            {
                if (value) // turn the bit on if value is true; otherwise, turn it off
                    bits |= (1 << index);
                else
                    bits &= ~(1 << index);
            }
        }

        //      0101 x101
        //  &        1   
        //           x      (=0 za x=0 i !=0 za x=1)

        //      0101 x101
        //  |        1   
        //      0101 1101      

        //      0101 x101
        //  &   1111 0111   (~(1 << index), komplement 0)   
        //      0101 0101      


    }
}
