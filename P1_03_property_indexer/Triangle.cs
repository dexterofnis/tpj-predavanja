﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_03_property_indexer
{
    class Triangle
    {
        public Triangle()
        {
            // automatska inicijalizacija na 0 
        }

        public Triangle(int side1length = 0, int side2length = 0, int side3length = 0)
        {
            Side1Length = side1length;
            Side2Length = side2length;
            Side3Length = side3length;
        }

        public double Side1Length
        { get; set; }

        public double Side2Length
        { get; set; }

        public double Side3Length
        { get; set; }

        public virtual int XX   // moze da se overrideuje po potrebi
        { get; set; }

    }
}
