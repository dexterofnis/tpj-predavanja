﻿namespace _P4_03_Forms_ToDoList
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBindingExample = new System.Windows.Forms.Button();
            this.btnToDoDG = new System.Windows.Forms.Button();
            this.btnToDoLV = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnBindingExample
            // 
            this.btnBindingExample.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBindingExample.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBindingExample.Location = new System.Drawing.Point(12, 12);
            this.btnBindingExample.Name = "btnBindingExample";
            this.btnBindingExample.Size = new System.Drawing.Size(260, 100);
            this.btnBindingExample.TabIndex = 0;
            this.btnBindingExample.Text = "Binding example form";
            this.btnBindingExample.UseVisualStyleBackColor = true;
            this.btnBindingExample.Click += new System.EventHandler(this.btnBindingExample_Click);
            // 
            // btnToDoDG
            // 
            this.btnToDoDG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToDoDG.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToDoDG.Location = new System.Drawing.Point(12, 118);
            this.btnToDoDG.Name = "btnToDoDG";
            this.btnToDoDG.Size = new System.Drawing.Size(260, 100);
            this.btnToDoDG.TabIndex = 1;
            this.btnToDoDG.Text = "ToDo with DataGrid";
            this.btnToDoDG.UseVisualStyleBackColor = true;
            this.btnToDoDG.Click += new System.EventHandler(this.btnToDoDG_Click);
            // 
            // btnToDoLV
            // 
            this.btnToDoLV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnToDoLV.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToDoLV.Location = new System.Drawing.Point(12, 224);
            this.btnToDoLV.Name = "btnToDoLV";
            this.btnToDoLV.Size = new System.Drawing.Size(260, 100);
            this.btnToDoLV.TabIndex = 2;
            this.btnToDoLV.Text = "ToDo with ListView";
            this.btnToDoLV.UseVisualStyleBackColor = true;
            this.btnToDoLV.Click += new System.EventHandler(this.btnToDoLV_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 336);
            this.Controls.Add(this.btnToDoLV);
            this.Controls.Add(this.btnToDoDG);
            this.Controls.Add(this.btnBindingExample);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnBindingExample;
        private System.Windows.Forms.Button btnToDoDG;
        private System.Windows.Forms.Button btnToDoLV;
    }
}