﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;

namespace _P4_03_Forms_ToDoList
{
    public class ToDoEntry
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
    }

    public class ToDoEntryNotify : INotifyPropertyChanged
    {

        public bool isNotifying { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string propertyName)
        {
            if (isNotifying)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ToDoEntryNotify()
        {
            DueDate = DateTime.Now;
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { _title = value; NotifyPropertyChanged("Title"); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; NotifyPropertyChanged("Description"); }
        }

        private DateTime _duedate;
        public DateTime DueDate
        {
            get { return _duedate; }
            set { _duedate = value; NotifyPropertyChanged("DueDate"); }
        }

    }
}
