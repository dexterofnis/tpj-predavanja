﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _P4_03_Forms_ToDoList
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnBindingExample_Click(object sender, EventArgs e)
        {
            BindingExampleForm frmBindingExample = new BindingExampleForm();
            frmBindingExample.Show();
        }

        private void btnToDoDG_Click(object sender, EventArgs e)
        {
            ToDoDG frmToDoDG = new ToDoDG();
            frmToDoDG.Show();
        }

        private void btnToDoLV_Click(object sender, EventArgs e)
        {
            ToDoLV frmToDoLV = new ToDoLV();
            frmToDoLV.Show();
        }
    }
}
