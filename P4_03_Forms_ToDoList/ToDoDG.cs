﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Xml.Serialization;
using System.IO;

namespace _P4_03_Forms_ToDoList
{
    public partial class ToDoDG : Form
    {
        private BindingList<ToDoEntry> entries = new BindingList<ToDoEntry>();

        // U okruzenju je podeseno dataGridView1.DataSource = entitiesSource pa se sve operacije na entitiesSource (pa cak i na entries) direktno prenose i na 
        // DataGridView. Vazi i obrnuto. Primetite da se u kodu nigde vise ne pominje DataGridView komponenta...

        public ToDoDG()
        {
            InitializeComponent();

            titleText.DataBindings.Add("Text", entriesSource, "Title", true, DataSourceUpdateMode.Never);   
            dueDatePicker.DataBindings.Add("Value", entriesSource, "DueDate", true, DataSourceUpdateMode.OnPropertyChanged);
            descriptionText.DataBindings.Add("Text", entriesSource, "Description", true, DataSourceUpdateMode.OnPropertyChanged);

            entriesSource.DataSource = entries;

            CreateNewItem();

            Console.WriteLine("DG forma aktivirana!");
        }

        private void CreateNewItem()
        {
            ToDoEntry newEntry = entriesSource.AddNew() as ToDoEntry;
            //ToDoEntry newEntry = new ToDoEntry();

            newEntry.Title = "New entry";
            newEntry.DueDate = DateTime.Now;
            newEntry.Description = "Desc.";

            entriesSource.ResetCurrentItem();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (entriesSource.Count > 0) 
                entriesSource.RemoveCurrent();      // brise selektovan element. Kada ga selektujemo u DataGridView, automatski se selektuje i u entitiesSource.

        }

        private void newButton_Click(object sender, EventArgs e)
        {
            CreateNewItem();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = @"d:\file.lst";

            // Pokretanje prozora za otvaranje fajla
            openFileDialog1.FileName = "";                      // Sugerisano ime fajla kad se otvori prozor
            openFileDialog1.Multiselect = false;                // Da li korisnik moze da odabere vise fajlova
            openFileDialog1.Filter = "List|*.lst";              // Ekstenzije koje da prikazuje. Format: <naziv>|<ext>|<naziv>|<ext>... 
            openFileDialog1.CheckFileExists = true;             // Da li da proverava da li fajl postoji. Kod otvaranja fajla naravno da treba.
            DialogResult res = openFileDialog1.ShowDialog();    // Prikazi dialog i ako korisnik ne klikne OK, izadji iz funkcije 
            if (res != DialogResult.OK)
                return;

            path = openFileDialog1.FileName;                    // FileName je puna putanja. Ukoliko zelite samo ime fajla, onda je to SafeFileName
    
            // Otvaranje fajla UVEK mora da ide pod try - catch
            try
            {
                // Klasa koja konvertuje kolekciju u XML dokument i obrnuto
                // Parametar konstruktora je tip koji se serializuje (moze da bude obicna klasa ili kolekcija).
                XmlSerializer serializer = new XmlSerializer(entries.GetType());
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    // Konvertuj (deserijalizuj) XML iz datog streama (u ovom slucaju FileStream, tj. file) 
                    // u promenljivu (kolekciju) entries. 
                    entries = serializer.Deserialize(fs) as BindingList<ToDoEntry>;
                    entriesSource.DataSource = entries;
                }
                MessageBox.Show("File loaded", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while opening file: " + ex.Message, "Oooops...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
         }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string path = @"d:\file.lst";

            saveFileDialog1.FileName = "";
            saveFileDialog1.Filter = "List|*.lst";
            saveFileDialog1.CheckFileExists = false;            // Ovde ne proveravamo da li fajl postoji, posto ne bi trebao da postoji :)...
            DialogResult res = saveFileDialog1.ShowDialog();
            if (res != DialogResult.OK)
                return;

            path = saveFileDialog1.FileName;

            // Slicno kao za otvaranje, samo sto se FileStream otvara za pisanje, i poziva se Serialize metoda
            try
            {
                XmlSerializer serializer = new XmlSerializer(entries.GetType());
                using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    serializer.Serialize(fs, entries);
                    MessageBox.Show("File saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while saving file: " + ex.Message, "Oooops...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();     // Izlazak iz programa
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            entries = new BindingList<ToDoEntry>();
            entriesSource.DataSource = entries;
        }

        private void titleText_Validating(object sender, CancelEventArgs e)
        {
            if (titleText.Text.Length > 10)
            {
                e.Cancel = true;
                titleText.Select(0, titleText.Text.Length);
                errorProvider1.SetError(titleText, "Name should not be longer than 10 characters!");
            }
        }

        private void titleText_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(titleText, "");
            titleText.DataBindings[0].WriteValue();         // Tek kada uspe validacija, vrednost moze da se upise
        }


    }
}
