﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _P4_03_Forms_ToDoList
{
    public partial class BindingExampleForm : Form
    {
        ToDoEntryNotify tden = new ToDoEntryNotify();
       
        public BindingExampleForm()
        {
            InitializeComponent();

            BindTo();
        }

        

        private void BindTo()
        {
            titleText.DataBindings.Clear();
            titleText.DataBindings.Add("Text", tden, "Title", false, DataSourceUpdateMode.OnPropertyChanged);

            descriptionText.DataBindings.Clear();
            descriptionText.DataBindings.Add("Text", tden, "Description", false, DataSourceUpdateMode.OnPropertyChanged);

            dueDatePicker.DataBindings.Clear();
            dueDatePicker.DataBindings.Add("Value", tden, "DueDate", false, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void btnDisplayMessage_Click(object sender, EventArgs e)
        {
            string message = "Title: " + tden.Title + "\n" +
                "DueDate: " + tden.DueDate.ToShortDateString() + "\n" +
                "Description: " + tden.Description;

            MessageBox.Show(message);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tden.Description = tden.Title = "";
            tden.DueDate = DateTime.Now;
        }

        private void ratioNoNotify_CheckedChanged(object sender, EventArgs e)
        {
            tden.isNotifying = radioNotify.Checked;
        }
    }
}
