﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace _P4_03_Forms_ToDoList
{
    static class Program
    {


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            // Projekat -> desni klik -> Properties -> Output type : promeniti u Console application, i dobijate konzolu paralelno sa formom
            //Console.WriteLine("This is from the main program");

            Application.Run(new MainForm());
        }
    }
}
