﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _P4_03_Forms_ToDoList
{
    public partial class ToDoLV : Form
    {
        private BindingList<ToDoEntry> entries = new BindingList<ToDoEntry>();

        public ToDoLV()
        {
            InitializeComponent();


            // DataBindings: kolekcija koja sadrzi objekte Binding klase. Svaki objekat realizuje povezivanje propertija neke komponente sa 
            // sa propertijem elementa neke kolekcije (koja predstavlja datasource).
            //
            // parametri metode Add: property komponente, izvor, property elementa izvora, formatiranje, DataSourceUpdateMode (najcesce OnPropertyChanged, 
            // sto znaci da se update radi kad god se desi bilo kakva promena)

            titleText.DataBindings.Add("Text", entriesSource, "Title", true, DataSourceUpdateMode.OnPropertyChanged);
            dueDatePicker.DataBindings.Add("Value", entriesSource, "DueDate", true, DataSourceUpdateMode.OnPropertyChanged);
            descriptionText.DataBindings.Add("Text", entriesSource, "Description", true, DataSourceUpdateMode.OnPropertyChanged);


            // BindingSource sluzi za povezivanje kolekcije sa nekom komponentom za DataBinding. 
            // DataSource property predstavlja kolekciju sa kojom se BindingSource povezuje.

            // Napomena: Ako se koristi obicna lista umesto BindingList, desava se da se ListChanged event okida 2x pri dodavanju komponente i slicni problemi.
            entriesSource.DataSource = entries;


            CreateNewItem();

            Console.WriteLine("LV forma aktivirana!");
        }

        private void CreateNewItem()
        {
            // Sve promene nad kolekcijom vrse se pomocu BindingSource (entitesSource) objekta. U ovom slucaju, kreiranje novog elementa.
            ToDoEntry newEntry = entriesSource.AddNew() as ToDoEntry;

            newEntry.Title = "New entry";
            newEntry.DueDate = DateTime.Now;
            newEntry.Description = "Desc.";

            entriesSource.ResetCurrentItem();
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            CreateNewItem();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            // SelectedIndices je kolekcija selektovanih redova u ListView. Posto je MultiSelect postavljeno na false, mogu biti selektovani 0 ili 1 el. 
            if (entriesListView.SelectedIndices.Count != 0)
            {
                int entryIndex = entriesListView.SelectedIndices[0];
                entriesSource.RemoveAt(entryIndex); // brisanje se obavlja pomocu entitiesSource komponenete.

                // Odredjuje selektovani indeks nakon brisanja
                if (entriesListView.Items.Count <= entryIndex)
                    entryIndex--;
                if (entryIndex >= 0)
                {
                    entriesListView.Items[entryIndex].Selected = true;
                    entriesListView.Focus();
                }
            }
        }


        // Update trenutne pozicije u entitiesSource. Kad se selektuje red u ListView, promeni se trenutni el. kolekcije, a samim tim i komponente (Title, Date, Desc.)
        // bindovane na njega. 
        private void entriesListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (entriesListView.SelectedIndices.Count != 0)
            {
                int entryIndex = entriesListView.SelectedIndices[0];
                entriesSource.Position = entryIndex;    // pozicija trenutnog elementa na koji su bindovane komponente...
            }
        }

        // Promena nekog od elemenata kolekcije. Parametar e nosi informaciju o tipu promene i indeksu elementa koji je promenjen. Ova metoda automatski updateuje
        // ListView na promene. Nakon ovoga, sve promene se vrse na BindingSource (entitiesSource) a automatski se reflektuju na ListView.
        private void entriesSource_ListChanged(object sender, ListChangedEventArgs e)
        {
            switch (e.ListChangedType)
            {
                case ListChangedType.ItemAdded:
                    MakeListViewItemForNewEntry(e.NewIndex);
                    break;
                case ListChangedType.ItemDeleted:
                    RemoveListViewItem(e.NewIndex);
                    break;
                case ListChangedType.ItemChanged:
                    UpdateListViewItem(e.NewIndex);
                    break;
            }
        }

        private void MakeListViewItemForNewEntry(int newItemIndex)
        {
            // Kolekcija Items sadrzi objekte tipa ListViewItem. Svaki item (red) ima SubItems koji predstavljaju vrednosti kolona za taj red 


            // Nakon kreiranja, kolekcija SubItems sadrzi jedan element koji je jednak "". Dodajemo jos dva prazna stringa posto imamo ukupno 3 kolone.
            ListViewItem item = new ListViewItem();         
            item.SubItems.Add(""); item.SubItems.Add("");

            entriesListView.Items.Insert(newItemIndex, item);
        }

        private void UpdateListViewItem(int itemIndex)
        {
            // Kada se promeni vrednost elementa kolekcije, updatuje vrednosti u ListView komponenti

            ListViewItem item = entriesListView.Items[itemIndex];
            ToDoEntry entry = entries[itemIndex]; // = entriesSource.Current as ToDoEntry
            item.SubItems[0].Text = entry.Title;
            item.SubItems[1].Text = entry.DueDate.ToShortDateString();
            item.SubItems[2].Text = entry.Description;
        }

        private void RemoveListViewItem(int deletedItemIndex)
        {
            entriesListView.Items.RemoveAt(deletedItemIndex);       // Brisemo red u ListView koji je odgovarao obrisanom elementu kolekcije...
        }
    }
}
