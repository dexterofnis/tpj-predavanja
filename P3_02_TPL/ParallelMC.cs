﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace _P3_02_TPL
{
    class ParallelMC
    {
        public static void Main()
        {
            int Ntotal = 50000000, Nfor = 100;

            double[] P = new double[Nfor];

            Stopwatch sw = Stopwatch.StartNew();
                
            // Serijsko izvrsavanje Nfor=100 poziva funkcije MCIntegrate
            // Ukupno Ntotal tacaka u Monte-Carlo integraciji
            // Niz P[i] pamti rezultat i-tog poziva funkkcije MCIntegrate
            //for (int i = 0; i < Nfor; i++)
            //    P[i] = MCIntegrate(Ntotal / Nfor);

            //double Pavg = P.Average();

            double Pavg = MCIntegrate(Ntotal);
            sw.Stop();

            Console.WriteLine("Serial:");
            Console.WriteLine("Total value on {0} samples is {1}", Ntotal, Pavg);
            Console.WriteLine("{0} miliseconds\n", sw.ElapsedMilliseconds);

            sw.Restart();
            // Paralelno izvrsavanje
            // parametri su pocetak, kraj, kao i delegat ciji je parametar
            // jedna promenljiva tipa integer
            Parallel.For(0, Nfor - 1, 
                (i) => P[i] = MCIntegrate(Ntotal / Nfor)
                );


            Pavg = P.Average();
            sw.Stop();

            Console.WriteLine("Parallel.For:"); 
            Console.WriteLine("Total value on {0} samples is {1}", Ntotal, Pavg);
            Console.WriteLine("{0} miliseconds\n", sw.ElapsedMilliseconds);

            double S1 = 0, S2 = 0, S3 = 0, S4 = 0;
            
            sw.Restart();
            Parallel.Invoke(
                () => { S1 = MCIntegrate(Ntotal / 2); },
                () => { S2 = MCIntegrate(Ntotal / 2); }
                );
            sw.Stop();
            
            Console.WriteLine("Parallel.Invoke with 2 threads:");
            Console.WriteLine("Total value on {0} samples is {1}", Ntotal, (S1 + S2) / 2);
            Console.WriteLine("{0} miliseconds\n", sw.ElapsedMilliseconds);

            sw.Restart();
            Parallel.Invoke(
                () => { S1 = MCIntegrate(Ntotal / 4); },
                () => { S2 = MCIntegrate(Ntotal / 4); },
                () => { S3 = MCIntegrate(Ntotal / 4); },
                () => { S4 = MCIntegrate(Ntotal / 4); }
            );
            sw.Stop();

            Console.WriteLine("Parallel.Invoke with 4 threads:");
            Console.WriteLine("Total value on {0} samples is {1}", Ntotal, (S1 + S2 + S3 + S4) / 4);
            Console.WriteLine("{0} miliseconds\n", sw.ElapsedMilliseconds);

        }

        // Funkcija koja racuna trostruki integral Monte-Carlo metodom.
        // Generise se ukupno N random tacaka na [0,1]x[0,1]x[0,1] i racuna se
        // [ f(x_1)+f(x_2)+...+f(x_N) ]/N
        // To je priblizna vrednost integrala fje f(x) na skupu [0,1]x[0,1]x[0,1]
        private static double MCIntegrate(int N)
        {
            double S = 0;

            Random rand = new Random();

            for (int i = 0; i < N; i++)
            {
                double
                    x = rand.NextDouble(),
                    y = rand.NextDouble(),
                    z = rand.NextDouble();

                S += Math.Exp(-x * y - y * z - z * x);
            }

            return S / N;
        }
    }
}
