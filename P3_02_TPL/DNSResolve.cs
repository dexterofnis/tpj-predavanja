﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;


namespace _P3_02_TPL
{
    public static class DNSResolve
    {
        private static List<string> hostnames = new List<string>() { "dexter_compact", "dexter_yoga", "VADER", "www.pmf.ni.ac.rs", "a1", "a2", "a3", "a4", "a5" };

        public static void DNSResolveSync()
        {
            DateTime startTime = DateTime.Now;

            foreach (string host in hostnames)
            {
                Console.WriteLine(host);

                try
                {
                    IPAddress[] addrs = Dns.GetHostAddresses(host);

                    foreach (IPAddress addr in addrs)
                        Console.WriteLine($"     {addr}"); 
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"     Error: {ex.Message}");
                }

                Console.WriteLine();
            }

            Console.WriteLine("All done in {0:F3} seconds", DateTime.Now.Subtract(startTime).TotalSeconds);
        }

        private static object callbackLock = new object();
        private static int nRemaining;
        private static AutoResetEvent are = new AutoResetEvent(false);

        public static void DNSResolveAsync()
        {
            DateTime startTime = DateTime.Now;

            nRemaining = hostnames.Count;

            foreach (string host in hostnames)
            {
                Dns.BeginGetHostAddresses(host, new AsyncCallback(PingCallback), host);
            }


            are.WaitOne();

            Console.WriteLine("All done in {0:F3} seconds", DateTime.Now.Subtract(startTime).TotalSeconds);
        }

        public static void PingCallback(IAsyncResult res)
        {
            string host = (string)res.AsyncState;


            lock (callbackLock)
            {
                Console.WriteLine(host);
                try
                {
                    IPAddress[] addrs = Dns.EndGetHostAddresses(res);

                    foreach (IPAddress addr in addrs)
                        Console.WriteLine($"     {addr}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"     Error: {ex.Message}");
                }

                Console.WriteLine();

                if (--nRemaining == 0)
                    are.Set();
            }
        }

    }
}
