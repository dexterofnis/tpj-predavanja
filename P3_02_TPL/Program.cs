﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace _P3_02_TPL
{
    class Program
    {

        static void Main(string[] args)
        {
            Taskovi();

            TaskFactory();

            TaskReturnValue();

            ParallelMC.Main();

            DNSResolve.DNSResolveSync();

            DNSResolve.DNSResolveAsync();
        }

        public static void doWork() => Console.WriteLine("Hello World");

        public static void doWork1(int i)
        {
            Console.WriteLine($"Hello World in parallel from process {i}");
            // pauzira proces na 500 milisekundi
            Thread.Sleep(500);
        }

        public static void Taskovi()
        {
            // kreiranje Task promenljivih
            // parametar konstruktora je void delegat bez parametara koji predstavlja
            // funkciju koja se izvrsava u tom tasku (procesu)

            Task task1 = new Task(doWork);
            Task task2 = new Task(() => doWork1(1));
            Task task3 = new Task(() => doWork1(2));

            // Startovanje procesa. Nakon ovoga se formira po jedan thread za svaki proces
            // a (glavni) program nastavlja izvrsavanje u svom threadu
            // Ako se zavrsi izvrsavanje glavnog programa, automatski se prekidaju svi threadovi
            task1.Start(); task2.Start(); task3.Start();

            // Kada izvrsenje drugog taska bude bilo gotovo, startuje se cetvrti
            Task newTask = task2.ContinueWith((x) => doWork1(3));


            // ceka dok se ne izvrse svi taskovi
            //task1.Wait(); task2.Wait(); task3.Wait(); newTask.Wait();
            Task.WaitAll(task1, task2, task3, newTask);
            // Slicno i Task.WaitAny()
        }

        public static void TaskFactory()
        {
            // kraci nacin za startovanje taskova
            Task task1 = Task.Factory.StartNew(() => doWork1(1));
            Task task2 = Task.Factory.StartNew(() => doWork1(2));


            Task cont = Task.Factory.ContinueWhenAll(new Task[] { task1, task2 },
                tasks =>
                {
                    foreach (Task task in tasks)
                        Console.WriteLine($"Task id {task.Id} completed!");
                });

            cont.Wait();
        }

        public static int SumIt(object v)
        {
            int x = (int)v;
            int sum = 0;
            for (; x > 0; x--)
                sum += x;
            return sum;
        }

        public static void TaskReturnValue()
        {
            Console.WriteLine("Main thread starting.");

            // Construct the first task.
            Task<bool> tsk = Task<bool>.Factory.StartNew(() => true);
            Console.WriteLine("After running MyTask. The result is: " + tsk.Result);

            // Construct the second task.
            Task<int> tsk2 = Task<int>.Factory.StartNew(SumIt, 3);
            Task<int> tsk3 = Task<int>.Factory.StartNew(() => SumIt(3));

            Console.WriteLine("After running SumIt. The result is: " + tsk2.Result);
            tsk.Dispose();
            tsk2.Dispose();

            Console.WriteLine("Main thread ending.");
        }



    }
}
