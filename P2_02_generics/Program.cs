﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BinarnoStablo;

namespace _P2_02_generics
{
    class Program
    {
        static void Main(string[] args)
        {
            NonGenericsDemo();

            GenericsDemo();

            TwoGenericsDemo();

            BaseClassConstraintDemo();

            PhoneListDemo();

            GenericCollections();

            // genericki metod
            F<int>(1, 2);
            
            // Moze i ovako :)
            F(1, 2);
            F(1.2, 2.3);
            F(new PhoneNumber("aaa", "1"), new PhoneNumber("bb", "1"));

            BinarnoStablo();

            CovarianceContravariance();

            Iteratori_demo();

        }

        private static void NonGenericsDemo()
        {
            NonGen iOb;

            // Create NonGen object.
            iOb = new NonGen(102);

            // Show the type of data stored in iOb.
            iOb.ShowType();

            // Get the value in iOb.
            // This time, a cast is necessary.
            int v = (int)iOb.GetOb();
            Console.WriteLine("value: " + v);
            Console.WriteLine();

            // Create another NonGen object and store a string in it.
            NonGen strOb = new NonGen("Non-Generics Test");

            // Show the type of data stored in strOb.
            strOb.ShowType();

            // Get the value of strOb.
            // Again, notice that a cast is necessary.
            string str = (string)strOb.GetOb();
            Console.WriteLine("value: " + str);

            // This compiles, but is conceptually wrong!
            iOb = strOb;
            // The following line results in a runtime exception.
            //v = (int) iOb.GetOb(); // runtime error!
        }

        private static void GenericsDemo()
        {
            // Create a Gen reference for int.
            Gen<int> iOb;

            // Create a Gen<int> object and assign its reference to iOb.
            iOb = new Gen<int>(102);

            // Show the type of data used by iOb.
            iOb.ShowType();

            // Get the value in iOb.
            int v = iOb.GetOb();
            Console.WriteLine("value: " + v);
            Console.WriteLine();

            // Create a Gen object for strings.
            Gen<string> strOb = new Gen<string>("Generics add power.");

            // Show the type of data stored in strOb.
            strOb.ShowType();

            // Get the value in strOb.
            string str = strOb.GetOb();
            Console.WriteLine("value: " + str);

            //iOb = strOb;

        }

        private static void TwoGenericsDemo()
        {
            TwoGen<int, string> tgObj = new TwoGen<int, string>(119, "Alpha Beta Gamma");

            // Show the types.
            tgObj.ShowTypes();

            // Obtain and show values.
            int v = tgObj.GetOb1();
            Console.WriteLine("value: " + v);

            string str = tgObj.GetObj2();
            Console.WriteLine("value: " + str);
        }

        private static void BaseClassConstraintDemo()
        {
            A a = new A();
            B b = new B();
            C c = new C();

            // The following is valid because A is the specified base class.
            Test<A> t1 = new Test<A>(a);
            t1.SayHello();

            // The following is valid because B inherits A.
            Test<B> t2 = new Test<B>(b);
            t2.SayHello();

            // The following is invalid because C does not inherit A.
            // Test<C> t3 = new Test<C>(c); // Error!
            // t3.SayHello(); // Error!
        }

        private static void PhoneListDemo()
        {
            // The following code is OK because Friend
            // inherits PhoneNumber.
            PhoneList<Friend> plist = new PhoneList<Friend>();
            plist.Add(new Friend("Tom", "555-1234", true));
            plist.Add(new Friend("Gary", "555-6756", true));
            plist.Add(new Friend("Matt", "555-9254", false));
            try
            {
                // Find the number of a friend given a name.
                Friend frnd = plist.FindByName("Gary");
                Console.Write(frnd.Name + ": " + frnd.Number);
                if (frnd.IsWorkNumber)
                    Console.WriteLine(" (work)");
                else
                    Console.WriteLine();
            }
            catch (NotFoundException)
            {
                Console.WriteLine("Not Found");
            }
            Console.WriteLine();
            
            // The following code is also OK because Supplier
            // inherits PhoneNumber.
            PhoneList<Supplier> plist2 = new PhoneList<Supplier>();
            plist2.Add(new Supplier("Global Hardware", "555-8834"));
            plist2.Add(new Supplier("Computer Warehouse", "555-9256"));
            plist2.Add(new Supplier("NetworkCity", "555-2564"));

            try
            {
                // Find the name of a supplier given a number.
                Supplier sp = plist2.FindByNumber("555-2564");
                Console.WriteLine(sp.Name + ": " + sp.Number);
            }
            catch (NotFoundException)
            {
                Console.WriteLine("Not Found");
            }
            // The following declaration is invalid because EmailFriend
            // does NOT inherit PhoneNumber.
            // PhoneList<EmailFriend> plist3 =
            // new PhoneList<EmailFriend>(); // Error!
        }

        private static void GenericCollections()
        {
            List<int> listint = new List<int>();
            
            listint.Add(10);
            listint.Add(20);

            int drugi = listint[1];

            listint.Remove(10);

            SortedDictionary<string, int> hash = new SortedDictionary<string, int>();
            hash["Uros"] = 2;
            hash["Marko"] = 5;
       }
        
        public static void F<T>(T a, T b)
        {
            Console.WriteLine($"{a} {b}");
        }

        private static void UbaciUstablo<TItem>(BinarnoStablo<TItem> stablo, params TItem[] data) where TItem : IComparable<TItem>
        {
            foreach (TItem x in data)
                stablo.Insert(x);
        }

        private static void BinarnoStablo()
        {
            BinarnoStablo<int> stablo = new BinarnoStablo<int>(10);

            stablo.Insert(5);
            stablo.Insert(7);
            stablo.Insert(20);
            stablo.Insert(3);
            UbaciUstablo<int>(stablo, 1, 2, 3, 4, 5, 6, 7, 8, 0);

            stablo.ObidjiStablo();

            foreach (int item in stablo)
                Console.WriteLine(item);
        }

        private static void CovarianceContravariance()
        {
            // Wrapper<object> x = new Wrapper<string>();
            // x.SetData(o)

            Wrapper<string> stringWrapper = new Wrapper<string>("BlaBla");
            IRetrieveWrapper<object> retrievedStringWrapper = stringWrapper;
            Console.WriteLine("Retrieved value is {0}", retrievedStringWrapper.GetData());

            Wrapper<object> objectWrapper = new Wrapper<object>();
            IStoreWrapper<string> storeObjectWrapper = objectWrapper;
            storeObjectWrapper.SetData("some string");
            Console.WriteLine("Stored value is {0}", objectWrapper.GetData());

            // object = string

            // objectWrapper = stringWrapper
            // OK: o = objectWrapper.GetData();
            // ne valja: objectWrapper.SetData(1); 


            // stringWrapper = objectWrapper
            // OK: stringWrapper.SetData("aaa");
            // ne valja: s = stringWrapper.GetData();

            objectWrapper.SetData(null);
            (objectWrapper as IStoreWrapper<string>).SetData("some string again");
        }

        public static void Iteratori_demo()
        {
            // Create an instance of the collection class
            DaysOfTheWeek week = new DaysOfTheWeek();

            // Iterate with foreach
            foreach (string day in week)
                Console.Write(day + " ");
            Console.WriteLine();

            // Powers of 2
            Console.Write("Powers of 2 are: ");
            foreach (int i in Iteratori.Power(2, 8))
                Console.Write($"{i} ");
            Console.WriteLine();

            // Divisors
            int num = 10927;
            Console.Write($"Divisors of {num} are: ");
            foreach (int i in Iteratori.Divisors(num))
                Console.Write($"{i} ");
            Console.WriteLine();
        }

    }
}
