﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P2_02_generics
{
    // A base class that stores a name and phone number.
    class PhoneNumber
    {
        public PhoneNumber(string n, string num)
        {
            Name = n;
            Number = num;
        }
       
        // Auto-implemented properties that hold a name and phone number.
        public string Number { get; set; }
        public string Name { get; set; }
    }

    // A class of phone numbers for friends.
    class Friend : PhoneNumber
    {
        public Friend(string n, string num, bool wk) :
            base(n, num)
        {
            IsWorkNumber = wk;
        }

        public bool IsWorkNumber { get; private set; }
        // ...
    }

    // A class of phone numbers for suppliers.
    class Supplier : PhoneNumber
    {
        public Supplier(string n, string num) :
            base(n, num) { }
        // ...
    }

    // PhoneList can manage any type of phone list
    // as long as it is derived from PhoneNumber.
    class PhoneList<T> 
        where T : PhoneNumber
//      where T : IComparable
//      where T : V
    {
        private T[] phList;
        private int end;
        private const int MaxN = 10;

        public PhoneList()
        {
            phList = new T[MaxN];
            end = 0;
        }
        
        // Add an entry to the list.
        public bool Add(T newEntry)
        {
            if (end == MaxN) return false;
            phList[end] = newEntry;
            end++;
            return true;
        }
        
        // Given a name, find and return the phone info.
        public T FindByName(string name)
        {
            // A solution without returning a specific exception...
            // phList.First((T x) => x.Name == name);
            
            for (int i = 0; i < end; i++)
            {
                // Name can be used because it is a member of
                // PhoneNumber, which is the base class constraint.
                if (phList[i].Name == name)
                    return phList[i];
            }
            // Name not in list.
            throw new NotFoundException();
        }
        
        // Given a number, find and return the phone info.
        public T FindByNumber(string number)
        {
            for (int i = 0; i < end; i++)
            {
                // Number can be used because it is also a member of
                // PhoneNumber, which is the base class constraint.
                if (phList[i].Number == number)
                    return phList[i];
            }
            // Number not in list.
            throw new NotFoundException();
        }
        // ...
    }

    // A custom exception that is thrown if a name or number is not found.
    class NotFoundException : Exception
    {
        /* Implement all of the Exception constructors. Notice that
        the constructors simply execute the base class constructor.
        Because NotFoundException adds nothing to Exception,
        there is no need for any further actions. */
        public NotFoundException() : base() { }
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string message, Exception innerException) :
            base(message, innerException)
        { }
        protected NotFoundException(
        System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) :
            base(info, context)
        { }
    }


}
