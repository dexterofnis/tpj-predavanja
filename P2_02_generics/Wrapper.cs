﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P2_02_generics
{

    /*  Covariance : 
     *  Ukoliko se tip T nalazi ISKLJUCIVO kao return tip metoda unutar interfejsa,
     *  moguce je staviti 'out' ispred naziva tipa da bi se omogucilo da se instanci na
     *  IRetrieveWrapper<object> promenljivu dodeli vrednost instance na 
     *  IRetrieveWrapper<string> promenljivu.
     */
    interface IRetrieveWrapper<out T>
    {
        T GetData();
    }

    /*  Contravariance : 
     *  Ukoliko se tip T nalazi ISKLJUCIVO kao tip parametara metoda unutar interfejsa
     *  moguce je staviti 'in' ispred naziva tipa da bi se omogucilo da se instanci na
     *  IStoreWrapper<string> promenljivu dodeli vrednost instance na 
     *  IStoreWrapper<object> promenljivu.     
     */
    interface IStoreWrapper<in T>
    {
        void SetData(T data);
    }


    class Wrapper<T> : IStoreWrapper<T>, IRetrieveWrapper<T>
    {
        private T storedData;

        public Wrapper()
        { }

        public Wrapper(T data)
        {
            storedData = data;
        }

        //public void IStoreWrapper<T>.SetData(T data)
        public void SetData(T data)
        {   
            // ukoliko je data tipa string, moze se dodeliti objektu, ali funkcija ne moze da vrati
            // objekat umesto stringa
            this.storedData = data;
        }
        
        //public T IRetrieveWrapper<T>.GetData()
        public T GetData()
        {
            // ukoliko se ocekuje da se vrati objekat, nije problem da se vrati string, ali ne moze
            // se stringu dodeliti objekat niti nad objektom izvrsavati sve string operacije
            return this.storedData;
        }
    }
}
