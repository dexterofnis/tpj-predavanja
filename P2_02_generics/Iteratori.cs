﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace _P2_02_generics
{
    public class DaysOfTheWeek : IEnumerable
    {
        string[] days = { "Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat" };

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < days.Length; i++)
            {
                yield return days[days.Length - i - 1];
            }
        }
    }


    static class Iteratori
    {
        // moze i samo metoda da vraca IEnumerable, a sa yield da se dodaju elementi
        public static IEnumerable<int> Power(int number, int exponent)
        {
            int counter = 0;
            int result = 1;
            while (counter++ < exponent)
            {
                result *= number;
                yield return result;
            }
        }

        public static IEnumerable Divisors(int number)
        {
            int div = 0;

            while (++div <= number)
            {
                if (number % div == 0)
                    yield return div;
            }
        }

    }
}
