﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P2_02_generics
{
    // A simple generic class.
    using System;
    
    // NonGen is functionally equivalent to Gen but does not use generics.
    class NonGen
    {
        object ob; // ob is now of type object
        
        // Pass the constructor a reference of type object.
        public NonGen(object o)
        {
            ob = o;
        }

        // Return type object.
        public object GetOb()
        {
            return ob;
        }

        // Show type of ob.
        public void ShowType()
        {
            Console.WriteLine("Type of ob is " + ob.GetType());
        }
    }




    // In the following Gen class, T is a type parameter
    // that will be replaced by a real type when an object
    // of type Gen is created.
    class Gen<T>
    {
        T ob; // declare a variable of type T
        // Notice that this constructor has a parameter of type T.
        public Gen(T o)
        {
            ob = o;
        }

        // Return ob, which is of type T.
        public T GetOb()
        {
            return ob;
        }

        // Show type of T.
        public void ShowType()
        {
            Console.WriteLine("Type of T is " + ob.GetType());
        }
    }

    class TwoGen<T, V>
    {
        T ob1;
        V ob2;
        // Notice that this constructor has parameters of type T and V.
        public TwoGen(T o1, V o2)
        {
            ob1 = o1;
            ob2 = o2;
        }
        // Show types of T and V.
        public void ShowTypes()
        {
            Console.WriteLine("Type of T is " + typeof(T));
            Console.WriteLine("Type of V is " + typeof(V));
        }
        public T GetOb1()
        {
            return ob1;
        }
        public V GetObj2()
        {
            return ob2;
        }
    }

 
}
