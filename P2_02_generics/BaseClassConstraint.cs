﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P2_02_generics
{
    class A
    {
        public void Hello()
        {
            Console.WriteLine("Hello");
        }
    }

    // Class B inherits A.
    class B : A { }

    // Class C does not inherit A.
    class C { }

    // Because of the base class constraint, all type arguments
    // passed to Test must have A as a base class.
    class Test<T> 
        where T : A, new()
        //  where T : IComparable
        //  where T : V
    {
        T obj;
        
        public Test(T o)
        {
            obj = o;
        }

        public void SayHello()
        {
            // OK to call Hello() because it’s declared
            // by the base class A.
            obj.Hello();
        }
    }


}
