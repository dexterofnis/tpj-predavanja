﻿using System;
using System.Linq;
using System.Text;

namespace _03_nizovi_kolekcije_stringovi
{
    class Program
    {

        static void Main(string[] args)
        {
            // deklaracija i inicijalizacija nizova
            BasicArrays();

            // prolazak kroz niz i kopiranje nizova
            IteratingThroughArray();

            // matrice
            MultidimensionalArrays();

            ParamArrays();

            // stringovi
            Strings();
        }

         private static void BasicArrays()
        {
            int[] a = new int[4];
            int[] aa = null;

            // indeksiranje ide od 0
            a[0] = 1;
            a[1] = a[0] + 1;
            
            // duzina niza ne mora da bude konstantna - dinamicki nizovi
            int n = 101;
            int[] b = new int[n];

            // inicijalizacija elemenata niza konstantnim vrednostima
            int[] pins = new int[4] { 9, 3, 7, 2 };
            
            // inicijalizacija elemenata niza ne-konstantnim vrednostima
            Random r = new Random();
            int[] c = new int[4] { r.Next(), r.Next(), r.Next(), r.Next() };

            // automatski odredjuje tip...
            var names = new[] { "John", "Diana", "James", "Francesca" };

            try
            {
                Console.WriteLine($"{pins[0]}, {pins[3]}");
                Console.WriteLine(pins[4]); // error, the 4th and last element is at index 3
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void IteratingThroughArray()
        {
            int[] a = new int[4] { 1, 2, 3, 4 };

            
            // prolazak kroz niz klasicnom for petljom
            for (int i = 0; i <= a.Length - 1; i++)
                Console.Write($"{a[i]} ");
            Console.Write('\n');

            // foreach
            foreach (int element in a)
                Console.Write($"{element} ");
            Console.Write('\n');

            // kopiranje niza na 3 nacina
            int[] b = new int[a.Length];
            a.CopyTo(b, 0); // startni indeks prvog niza
            Array.Copy(a, b, a.Length);
            int[] c = (int[])a.Clone();

            // prosiri niz c na c.Length * 2 elemenata
            Array.Resize(ref c, c.Length * 2);
                   
            Console.WriteLine($"{c[1]} i {a[1]}");
        }

        private static void MultidimensionalArrays()
        {
            int[,] items = new int[4, 6];

            items[2, 3] = 99; // set the element at cell(2,3) to 99
            items[2, 4] = items[2, 3]; // copy the element in cell(2, 3) to cell(2, 4)
            items[2, 4]++; // increment the integer value at cell(2, 4)


            int[,,] cube = new int[5, 5, 5];
            cube[1, 2, 1] = 101;
            // ne moze ovako : cube[1][2][1] = 1111;
            cube[1, 2, 2] = cube[1, 2, 1] * 3;



            var nonZeroBasedArray = Array.CreateInstance(
                Type.GetType("System.Int32"), 
                new int[] { 5, 5 }, 
                new int[] { -1, -2 });

            nonZeroBasedArray.SetValue(3, -1, -1);
            int a = (int)nonZeroBasedArray.GetValue(-1, -1);
        }


        private static void ParamArrays()
        {
            // parameter arrays
            Console.WriteLine(Min(1, 2, 3, 4) + Min(1, 2) + Min(1) + Min(1, 2, 3));
            Console.WriteLine(Min());   // moze i bez parametara
            Console.WriteLine(Min(new int[] { 1, 2, 3, 4 }));

            // nizovi kao parametri funkcija
            int[] a = new int[10];
            FillWithOnes(a);            // prenos je po referenci
            Console.WriteLine(a[0]);
        }

        private static void FillWithOnes(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
                a[i] = 1;
        }

        private static int Min(params int[] paramList)
        {
            int min = int.MaxValue;
            
            for (int i = 0; i < paramList.Length; i++)
                if (paramList[i] < min)
                    min = paramList[i];

            return min;
        }

        private static void Strings()
        {
            string str1 = "C# is the best programming language";
            string str11 = @"\\\\\";        // ovo je \\\\\
            string str12 = "\\\\"; // ovo je \\
            
            Console.WriteLine(str1.ToUpper());
            Console.WriteLine(str1.ToLower());

            //str1[1] = 'a';  //-- ne moze!!!
            
            char ch = str1[1];

            string sub = str1.Substring(0, 2);
            string str2 = str1.Insert(1, "aaa");
            string str3 = str2.Remove(1, 3);
            string str23 = str2 + str3;


            string str4 = "123abc456xyz789";
            Console.WriteLine("------ Trim demonstration ----- \n Original: {0}\n TrimStart: {1}\n TrimEnd: {2}\n Trim: {3}\n",
                str4,
                str4.TrimStart('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),   // sa pocetka
                str4.TrimEnd('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),     // sa kraja
                str4.Trim('1', '2', '3', '4', '5', '6', '7', '8', '9', '0')         // i sa pocetka i sa kraja
                );

            string line = "12  32   4   2    1   2           3";

            string[] parts = line.Split(' ', '\t');
            parts = line.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            int s = 0; 
            foreach (string part in parts)
                s += int.Parse(part);
            Console.WriteLine("Suma svih brojeva u stringu je : {0}", s);

            string ss = "1aaa";
            int x;
            if (!int.TryParse(ss, out x))
                // formatted strings, tnx Danilo :)
                // Console.WriteLine("String {0} nije ceo broj!!", ss);
                Console.WriteLine($"String {ss} nije ceo broj!!");

            StringBuilder sb = new StringBuilder();

            sb.Append(10);
            sb.Append(' ', 20);
            sb.Append("dasjdaiso");
            sb.Remove(0, 1);
            sb[2] = '#';

            Console.WriteLine(sb.ToString());
        }

    }
}
