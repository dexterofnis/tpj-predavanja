﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _P4_08_EntityFramework_SQLITE
{
    // Staticka klasa koja omogucava komunikaciju sa bazom putem Entity Framework-a
    // Ovde se formira i cuva osnovni objekat tipa SmallBaseEntities
    public static class DataUtilities
    {
        public static SmallBaseEntities sbe = new SmallBaseEntities();

        public static List<Product> GetProducts() => sbe.Products.ToList();
        public static List<Customer> GetCustomers() => sbe.Customers.ToList();
        public static List<CustomerProduct> GetCustomerProducts() => sbe.CustomerProducts.ToList();

        public static bool AddCustomerProduct(CustomerProduct cp)
        {
            // Dodaje objekat u kolekciju i poziva SaveChanges() da update-uje promene u bazi

            sbe.CustomerProducts.Add(cp);
            return SaveChanges();
        }
                
        // Poziva SaveChanges() metodu unutar try-catch bloka
        // Ako su vam potrebni detalji zasto je transakcija neuspesna, "uhvatite" specificni exception
        public static bool SaveChanges()
        {
            try
            {
                sbe.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // Metoda koja prevodi Model u ViewModel
        // Pretpostavljamo npr. da je celokupna kolicina iz jednog paketa 
        // Ova metoda se ne koristi u projektu, ali daje primer kako bi vrsili
        // ovo pretvaranje

        public static void CustomerProductModelToViewModel(CustomerProduct cpModel, CustomerProductViewModel cpViewModel)
        {
            cpViewModel.Customer = cpModel.Customer;
            cpViewModel.Product = cpModel.Product;
            cpViewModel.QuantityPerPackage = cpModel.Quantity;
            cpViewModel.NumberOfPackages = 1;
        }


        // Metoda koja prevodi ViewModel u Model
        // U konkretnom slucaju, samo iskopira property-je
        public static void CustomerProductViewModelToModel(CustomerProductViewModel cpViewModel, CustomerProduct cpModel)
        {
            cpModel.Customer = cpViewModel.Customer;
            cpModel.Product = cpViewModel.Product;
            cpModel.Quantity = cpViewModel.Quantity;
        }


    }
}
