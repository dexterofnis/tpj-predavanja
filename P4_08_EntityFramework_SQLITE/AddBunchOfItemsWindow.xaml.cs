﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _P4_08_EntityFramework_SQLITE
{
    /// <summary>
    /// Interaction logic for AddBunchOfItemsWindow.xaml
    /// </summary>
    public partial class AddBunchOfItemsWindow : Window
    {
        private CustomerProductViewModel cpvm;
        private Random rnd = new Random();

        public AddBunchOfItemsWindow(CustomerProductViewModel cpvm)
        {
            InitializeComponent();

            this.cpvm = cpvm;
            this.DataContext = cpvm;

            var listCustomers = DataUtilities.GetCustomers();
            this.cmbCustomer.ItemsSource = listCustomers;


            var listProducts = DataUtilities.GetProducts();
            this.cmbProduct.ItemsSource = listProducts;

            cpvm.Customer = listCustomers.First();
            cpvm.Product = listProducts.First();


            Validation.ClearInvalid(tbNumberOfPackages.GetBindingExpression(TextBox.TextProperty));

            Validation.ClearInvalid(tbQuantityPerPackage.GetBindingExpression(TextBox.TextProperty));
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid(tbNumberOfPackages))
                if (IsValid(tbQuantityPerPackage))
                {

                    this.DialogResult = true;
                    this.Close();
                }
        }

        private bool IsValid(Control ctrl)
        {
            if (Validation.GetErrors(ctrl).Count() == 0)
            {
                return true;
            }
            else
            {
                ValidationError error = Validation.GetErrors(ctrl)[0];
                MessageBox.Show(error.ErrorContent.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return false;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
               

        private void btnRandom_Click(object sender, RoutedEventArgs e)
        {
            cpvm.QuantityPerPackage = rnd.Next(1, 10);
            cpvm.NumberOfPackages = rnd.Next(1, 10);
            cmbCustomer.SelectedIndex = rnd.Next(0, cmbCustomer.Items.Count);
            cmbProduct.SelectedIndex = rnd.Next(0, cmbProduct.Items.Count);
        }
    }
}
