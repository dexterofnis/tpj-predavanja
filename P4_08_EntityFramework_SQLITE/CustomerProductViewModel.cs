﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace _P4_08_EntityFramework_SQLITE
{
    public class NotifyPropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(
             [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    // ViewModel je sprega izmedju modela i korisnickog interface-a (View)
    // Konkretno, u modelu imamo samo ukupan broj jedinica proizvoda (Quantity), 
    // a korisnik zeli da specificira broj paketa i broj jedinica po paketu.
    // Ove property-je sadrzi ViewModel. Pored toga, ViewModel implementira
    // INotifyPropertyChanged event koji omogucava DataBinding u smeru od source-a
    // do kontrola (sto je potrebno npr. za dugme Random).

    // Konverzija iz Modela u ViewModel i obrnuto obavlja se pomocu dve staticke
    // metode u DataUtilities klasi.

    // Ovo je samo jedan primer koriscenja ovog pattern-a, mnoge druge primere i
    // predloge mozete pronaci na internetu
    
    public class CustomerProductViewModel : NotifyPropertyChanged
    {

        private Customer _customer;
        public Customer Customer
        {
            get { return _customer; }
            set { _customer = value;  OnPropertyChanged(); }
        }

        private Product _product;
        public Product Product
        {
            get { return _product; }
            set { _product = value; OnPropertyChanged(); }
        }

        private long _quantityperpackage;
        public long QuantityPerPackage
        {
            get { return _quantityperpackage; }
            set {
                _quantityperpackage = value;
                OnPropertyChanged();
                OnPropertyChanged("Quantity"); // Promena ovog property-ja znaci i promenu Quantity property-ja
            }
        }

        private long _numberofpackages;
        public long NumberOfPackages
        {
            get { return _numberofpackages; }
            set {
                _numberofpackages = value;
                OnPropertyChanged();
                OnPropertyChanged("Quantity"); // Promena ovog property-ja znaci i promenu Quantity property-ja
            }
        }

        public long Quantity
        {
            get { return NumberOfPackages * QuantityPerPackage; }
        }

    }
}
