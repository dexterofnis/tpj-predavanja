﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace _P4_08_EntityFramework_SQLITE
{
    /// <summary>
    /// Interaction logic for AddNewItemWindow.xaml
    /// </summary>
    public partial class AddNewItemPackageWindow : Window
    {
        
        public AddNewItemPackageWindow(CustomerProduct cpnew, bool update)
        {
            InitializeComponent();

            this.DataContext = cpnew;

            var listCustomers = DataUtilities.GetCustomers();
            this.cmbCustomer.ItemsSource = listCustomers;


            var listProducts = DataUtilities.GetProducts();
            this.cmbProduct.ItemsSource = listProducts;

            if (!update)
            {
                cpnew.Customer = listCustomers.First();
                cpnew.Product = listProducts.First();
            }

            Validation.ClearInvalid(tbQuantity.GetBindingExpression(TextBox.TextProperty));
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            // Validation.GetErrors vraca greske za konkretni objekat
            if (Validation.GetErrors(tbQuantity).Count() == 0)
            {
                this.DialogResult = true;
                this.Close();
            }
            else
            {
                // ukoliko ima gresaka, prikazi prvu
                ValidationError error = Validation.GetErrors(tbQuantity)[0];
                MessageBox.Show(error.ErrorContent.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
