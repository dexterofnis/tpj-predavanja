﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace _P4_08_EntityFramework_SQLITE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        public MainWindow()
        {
            InitializeComponent();

            Refresh();               
        }

        private void Refresh()
        {
            //dgCustomerProduct.ItemsSource = null;
            dgCustomerProduct.ItemsSource = DataUtilities.GetCustomerProducts();
        }

        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {
            CustomerProduct cp = new CustomerProduct();

            AddNewItemPackageWindow addnewitem = new AddNewItemPackageWindow(cp, false);

            if (addnewitem.ShowDialog() == true)
            {
                if (DataUtilities.AddCustomerProduct(cp))
                    MessageBox.Show("Item successfully added", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show("Error adding item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                Refresh();
            }
        }

        private void dgCustomerProduct_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CustomerProduct cp = dgCustomerProduct.SelectedItem as CustomerProduct;

            AddNewItemPackageWindow addnewitem = new AddNewItemPackageWindow(cp, true);

            if (addnewitem.ShowDialog() == true)
            {
                if (DataUtilities.SaveChanges())
                    MessageBox.Show("Item successfully updated", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show("Error updating item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                Refresh();
            }


        }

        private void btnAddBunchItem_Click(object sender, RoutedEventArgs e)
        {
            CustomerProductViewModel cpvm = new CustomerProductViewModel();

            AddBunchOfItemsWindow addbunch = new AddBunchOfItemsWindow(cpvm);

            if (addbunch.ShowDialog()== true)
            {
                // Kopiramo ViewModel u Model
                CustomerProduct cp = new CustomerProduct();
                DataUtilities.CustomerProductViewModelToModel(cpvm, cp);

                // Dodajemo kreirani model u bazu
                if (DataUtilities.AddCustomerProduct(cp))
                    MessageBox.Show("Item successfully added", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show("Error adding item", "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                Refresh();
            }
        }
    }
}
