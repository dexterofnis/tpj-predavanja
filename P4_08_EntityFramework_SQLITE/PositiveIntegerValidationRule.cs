﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace _P4_08_EntityFramework_SQLITE
{

    // Validadator za pozitivni ceo broj
    public class PositiveIntegerValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int val;

            if (int.TryParse(value as string, out val))
                if (val > 0)
                    return new ValidationResult(true, null);        // ukoliko je value pozitivni ceo broj (int) vraca true

            return new ValidationResult(false, "Quantity must be positive integer"); // u suprotnom vraca false uz odgovarajucu poruku
        }
    }
}



