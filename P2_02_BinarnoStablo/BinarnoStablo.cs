﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BinarnoStablo
{
 
    public class BinarnoStablo<TItem> : IEnumerable<TItem> 
        where TItem : IComparable<TItem>
    {
        TItem nodeData;

        public BinarnoStablo<TItem> LeftTree { get; set; }
        public BinarnoStablo<TItem> RightTree { get; set; }
        public TItem NodeData
        {
            get { return nodeData; }
            set { nodeData = value; }
        }


        public BinarnoStablo(TItem element)
        {
            nodeData = element;
            LeftTree = RightTree = null;
        }

        public void Insert(TItem element)
        {
            if (nodeData.CompareTo(element) > 0)
            {
                if (LeftTree == null)
                    LeftTree = new BinarnoStablo<TItem>(element);
                else
                    LeftTree.Insert(element);
            }
            else
            {
                if (RightTree == null)
                    RightTree = new BinarnoStablo<TItem>(element);
                else
                    RightTree.Insert(element);
            }

        }

        private void Obidji()
        {
            if (LeftTree != null) LeftTree.Obidji();
            Console.Write($"{nodeData} ");
            if (RightTree != null) RightTree.Obidji();
        }

        public void ObidjiStablo()
        {
            Obidji(); 
            Console.WriteLine();
        }

        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
        {
            if (LeftTree != null)
            {
                foreach (TItem item in LeftTree)
                    yield return item;
            }

            yield return nodeData;

            if (RightTree != null)
            {
                foreach (TItem item in RightTree)
                    yield return item;
            }
        }

        // Wrapper, mora da se navede jer genericki interfejs nasledjuje negenericki
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (this as IEnumerable<TItem>).GetEnumerator();
        }


    }



}
