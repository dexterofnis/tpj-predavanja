﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace BinarnoStabloEnum
{
    class TreeEnumerator<TItem> : IEnumerator<TItem> where TItem : IComparable<TItem>
    {
        private BinarnoStablo<TItem> currentData = null;
        private TItem currentItem = default(TItem);
        private Queue<TItem> enumData = null;

        // ove dve metode moraju (formalno) da se implementiraju, zato sto IEnumerator<TItem> nasledjuje IEnumerator, pa i sve njegove clanove

        object IEnumerator.Current
        {
            get { throw new NotImplementedException(); }
        }
        void IEnumerator.Reset()
        {
            throw new NotImplementedException();
            //populate(enumData, currentData);
        }
        public TreeEnumerator(BinarnoStablo<TItem> data) 
        {
            this.currentData = data;
        }

        private void populate(Queue<TItem> enumQueue, BinarnoStablo<TItem> tree)
        {
            if (tree.LeftTree != null)
            {
                populate(enumQueue, tree.LeftTree);
            }
            enumQueue.Enqueue(tree.NodeData);
            if (tree.RightTree != null)
            {
                populate(enumQueue, tree.RightTree);
            }
        }

        TItem IEnumerator<TItem>.Current
        {
            get
            {
                if (this.enumData == null)
                    throw new InvalidOperationException
                    ("Use MoveNext before calling Current");
                return this.currentItem;
            }
        }

        bool System.Collections.IEnumerator.MoveNext()
        {
            if (this.enumData == null)
            {
                this.enumData = new Queue<TItem>();
                populate(this.enumData, this.currentData);
            }
            if (this.enumData.Count > 0)
            {
                this.currentItem = this.enumData.Dequeue();
                return true;
            }
            return false;
        }

        void IDisposable.Dispose()
        { }
    }


    public class BinarnoStablo<TItem> : IEnumerable<TItem> where TItem : IComparable<TItem>
    {
        TItem nodeData;

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }


        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
        {
            return new TreeEnumerator<TItem>(this);
        }

        public BinarnoStablo<TItem> LeftTree { get; set; }
        public BinarnoStablo<TItem> RightTree { get; set; }
        public TItem NodeData
        {
            get { return nodeData; }
            set { nodeData = value; }
        }


        public BinarnoStablo(TItem element)
        {
            nodeData = element;
            LeftTree = RightTree = null;
        }

        public void Insert(TItem element)
        {
            if (nodeData.CompareTo(element) > 0)
            {
                if (LeftTree == null)
                    LeftTree = new BinarnoStablo<TItem>(element);
                else
                    LeftTree.Insert(element);
            }
            else
            {
                if (RightTree == null)
                    RightTree = new BinarnoStablo<TItem>(element);
                else
                    RightTree.Insert(element);
            }

        }

        private void Obidji()
        {
            if (LeftTree != null) LeftTree.Obidji();
            Console.Write("{0} ", nodeData);
            if (RightTree != null) RightTree.Obidji();
        }

        public void ObidjiStablo()
        {
            Obidji(); 
            Console.WriteLine();
        }
    }   



}
