﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace _16_Regex
{
    class Program
    {

        //Tutorijali:
        //  elementaran: http://regexone.com/
        //  napredniji: http://www.regular-expressions.info/
        //  Microsoft: https://msdn.microsoft.com/en-us/library/az24scfc(v=vs.110).aspx

        //        Isprobavac (.NET based): http://regexlib.com/RETester.aspx?AspxAutoDetectCookieSupport=1 
        //                                 (nije free, izbacuje poruke) http://regexhero.net/tester/
        //Primeri: http://wiki.tcl.tk/989

        static void Main(string[] args)
        {
            WriteMatch();

            WriteMatches();

            WriteMatchGroups();

            WriteNumbers();

            MatchIPAddr();

        }

        private static void WriteMatch()
        {
            string input = "Marko Marko Petkovic";
            string pattern = @"Marko";

            Console.WriteLine("---------------------------\nInput: {0}\nPattern: {1}\n", input, pattern);
            
            Match mtch = Regex.Match(input, pattern);
            Console.WriteLine("Match: {0}\nIndex: {1}", mtch.Value, mtch.Index);

            Console.WriteLine("---------------------------\n\n");
        }

        private static void WriteMatches()
        {
            string input = "Marko Marko Petkovic";
            string pattern = @"Marko";

            Console.WriteLine("---------------------------\nInput: {0}\nPattern: {1}\n\nValue + index", input, pattern);

            MatchCollection mc = Regex.Matches(input, pattern);
            foreach (Match mtch in mc)
            {
                Console.WriteLine("{0} + {1}", mtch.Value, mtch.Index);
            }

            Console.WriteLine("---------------------------\n\n");
        }

        private static void WriteMatchGroups()
        {
            string input = "   Marko Marko Petkovic";
            string pattern = @"([a-zA-Z]+)\s\1"; // rec koja se javlja dva puta a izmedju pojavljivanja moze biti jedan space

            Console.WriteLine("---------------------------\nInput: {0}\nPattern: {1}\n", input, pattern);

            Match mtch = Regex.Match(input, pattern);
            Console.WriteLine("Match (value + index): {0} + {1}", mtch.Value, mtch.Index);
            Console.WriteLine("Match group 1 (value + index): {0} + {1}", mtch.Groups[1].Value, mtch.Groups[1].Index);

            Console.WriteLine("---------------------------\n\n");
        }

        private static void WriteNumbers()
        {
            string input = "daads 1.2 ads 33 asd-3443d d as 1e-10 qi9q0i das+1.3e20ee -10 -20 3.443 3e3";
            string pattern = @"[+-]?\d+((\.\d+)?([eE][+-]?\d+)?)?";

            Console.WriteLine("---------------------------\nInput: {0}\nPattern: {1}\nNumbers:", input, pattern);
            
            foreach (Match mtch in Regex.Matches(input,pattern))
            {
                Console.WriteLine(mtch.Value);
            }

            Console.WriteLine("---------------------------\n\n");
        }

        private static void MatchIPAddr()
        {
            // Original: (\[0-9]{1,3})\.(\[0-9]{1,3})\.(\[0-9]{1,3})\.(\[0-9]{1,3})

            Regex regIP = new Regex(@"^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$");
            List<string> inputs = new List<string> { "245.254.253.2", "1112.34.2.1", "1.1.1.1", "Marko 245.254.253.2", "999.999.999.999" };

            foreach (string input in inputs)
            {
                Match mtch = regIP.Match(input);
                
                if (mtch.Success)
                {
                    Console.WriteLine("{0} {1} {2} {3}", mtch.Groups[1].Value, mtch.Groups[2].Value, mtch.Groups[3].Value, mtch.Groups[4].Value);
                }
            }
        }


    }
}
