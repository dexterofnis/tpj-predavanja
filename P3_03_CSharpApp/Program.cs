﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;
using System.Diagnostics;

namespace P3_03_CSharpApp
{
    class Program
    {
        
        [DllImport("P3_03_DLLApp.dll",
            //EntryPoint = @"?AddTwo@@YAHHH@Z",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern int AddTwo(int a, int b);

        [DllImport("P3_03_DLLApp.dll",
            //EntryPoint = @"?Sort@@YAXPAHH@Z",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern void Sort(int[] a, int n);

        [DllImport("P3_03_DLLApp.dll",
           // EntryPoint = @"?GenerateRandomSequence@@YAXPAHH@Z",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern void GenerateRandomSequence(int[] a, int n);

        [DllImport("P3_03_DLLApp.dll",
            //EntryPoint = @"?Sort@@YAXPAHH@Z",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern int SumElMatrix(int[,] a, int m, int n);

        [DllImport("P3_03_DLLApp.dll",
            EntryPoint = "?MyFunction@@YAXAAH0@Z",
            CallingConvention = CallingConvention.Cdecl)]
        public static extern void MyFunction(ref int a, ref int b);

        static void Main(string[] args)
        {
            PrenosenjeProstihTipova();

            PrenosenjeNiza();

            PrenosenjeMatrice();

            SortTest();

        }

        private static void PrenosenjeProstihTipova()
        {
            // Sabiranje 2 broja pozivom AddTwo funkcije iz DLL-a
            int ret;
            Console.Write("1+8 = ");
            ret = AddTwo(1, 8);
            Console.WriteLine(ret);
        }

        private static void PrenosenjeNiza()
        {
            // Generisanje random niza
            int n = 5;
            int[] a = new int[n];
            
            GenerateRandomSequence(a, n);

            Console.Write("Random niz je : ");

            for (int i = 0; i < n; i++)
                Console.Write("{0} ", a[i]);
            Console.WriteLine();
        }

        private static void PrenosenjeMatrice()
        {
            // Suma elemenata matrice
            int m = 4, n = 10;  

            int[,] mat = new int[m, n];

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    mat[i, j] = i;

            // (0 + 1 + 2 + 3) * 10
            Console.WriteLine("Suma elemenata matrice je : {0}", SumElMatrix(mat, m, n));
        }

        private static void SortTest()
        {
            // Sortiranje niza pozivom Sort funkcije iz DLL-a
            // Ispisuje se vreme izvrsenja funkcije u milisekundama

            Console.WriteLine("\n\n--- Sort Testing ---\n");

            int n = 50000;
            int[] a = new int[n];
            GenerateRandomSequence(a, n);

            int[] b = new int[n];
            Array.Copy(a, b, n);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Sort(a, n);
            sw.Stop();
            Console.WriteLine("Unmanaged DLL from C# : {0}", sw.ElapsedMilliseconds);

            sw = new Stopwatch();
            sw.Start();
            SortCSharp(b);
            sw.Stop();
            Console.WriteLine("Native C# : {0}", sw.ElapsedMilliseconds);
        }

        static void SortCSharp(int[] a)
        {
            int n = a.Length;

            for (int i = 0; i < n - 1; i++)
                for (int j = i + 1; j < n; j++)
                    if (a[i] > a[j])
                    {
                        int tmp = a[i];
                        a[i] = a[j];
                        a[j] = tmp;
                    }
        }
    }
}
