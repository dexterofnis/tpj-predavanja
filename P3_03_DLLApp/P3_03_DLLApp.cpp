// P3_03_DLLApp.cpp : Defines the exported functions for the DLL application.
//

//#include "stdafx.h"

#include <stdlib.h>

void swap(int &a, int &b)
{
	int tmp=a; a=b; b=tmp;
}

int** DynMatrix(int *a, int m, int n)
{
	int **mat = new int*[m];

	for (int i=0; i<m; i++)
		mat[i] = (a+i*n);

	return mat;
}


extern "C" __declspec(dllexport) int AddTwo(int a, int b)
{
	return a+b;
}


extern "C" __declspec(dllexport) void Sort(int* a, int n)
{
	for (int i=0; i<n-1; i++)
		for (int j=i+1; j<n; j++)
			if (a[i]>a[j]) {int tmp=a[i]; a[i]=a[j]; a[j]=tmp;}
}

extern "C" __declspec(dllexport) void GenerateRandomSequence(int *a, int n)
{
	for (int i=0; i<n; i++)
		a[i] = rand();
}

extern "C" __declspec(dllexport) int SumElMatrix(int *a, int m, int n)
{
	int t = 0, sum = 0;

	int **mat = DynMatrix(a, m, n);
		
	for (int i=0; i<m; i++)
		for (int j=0; j<n; j++)
			//sum += a[t++];
			sum += mat[i][j];

	delete [] mat;

	return sum;
}

extern __declspec(dllexport) void MyFunction(int &a, int &b)
{
	a = b = 1;
}