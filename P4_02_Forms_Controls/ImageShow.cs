﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _P4_02_Forms_Controls
{
    public partial class ImageShow : Form
    {
        // Dodavanje resursa u aplikaciju. U Solution Exploreru kliknete na projekat, pa Properties, pa sa desne strane Resources, pa onda u comboboxu gore
        // odaberete koji je tip resursa (string, slika, zvucni fajl, itd.) i onda dodajete. Ovako dodati resursi se direktno ubacuju u exe fajl aplikacije...

        
        Image[] myImages = new Image[6] {
            Properties.Resources.Chrysanthemum,     // Pristup resursima
            Properties.Resources.Desert,
            Properties.Resources.Koala,
            Properties.Resources.Lighthouse,
            Properties.Resources.Penguins,
            Properties.Resources.Tulips
        };

        // Trenutnu poziciju pamtimo kao Settings aplikacije. Ovo se pamti u spec. fajlu koji se smesta u ProgramData folderu za svaku aplikaciju
        // Settings-e (podesavanja) dodajete isto kao resurse, samo umesto Resources idete na Settings

        public ImageShow()
        {
            InitializeComponent();

            hScrollBar1.Maximum = myImages.Length - 1;
            hScrollBar1.Value = Properties.Settings.Default.Position;       // Pristup settings-u

            UpdateImage();
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            UpdateImage();
        }

        private void UpdateImage()
        {
            pictureBox1.Image = myImages[hScrollBar1.Value];
        }


        // Kada se zatvara forma update-uje se podesavanje...
        private void ImageShow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Position = hScrollBar1.Value;
            Properties.Settings.Default.Save();
        }
    }
}
