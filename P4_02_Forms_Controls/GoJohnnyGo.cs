﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _P4_02_Forms_Controls
{
    public partial class GoJohnnyGo : Form
    {
        public double totalSec = 0;
        DateTime startTime;
        public bool finished = false;

        public GoJohnnyGo()
        {
            InitializeComponent();
        }

        // Event koji se poziva pri svakom okidanju timera
        private void timer1_Tick(object sender, EventArgs e)
        {
            totalSec = DateTime.Now.Subtract(startTime).TotalSeconds;
            int max = (int)totalTime.Value;

            if (totalSec > max)
            {
                Stop();
            }
            else
                WriteTime();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void WriteTime()
        {
            int max = (int)totalTime.Value;     // Vrednost NumericUpDown komponente
            timeLabel.Text = "Prošlo: " + totalSec + " sekundi...";
            int val = (int)Math.Round(totalSec / max * progressBar1.Maximum);   // Vrednost ProgressBar-a je izmedju minimalne (koja je u ovom slucaju 0) i maksimalne

            // Hack za updateovanje vrednosti ProgressBar-a (ne mora na WinXP)
            // pogledati: http://stackoverflow.com/questions/977278/how-can-i-make-the-progress-bar-update-fast-enough
            progressBar1.Value = val;
            if (val > 0)
            {
                progressBar1.Value = val - 1;
                progressBar1.Value = val;
            }
            progressBar1.Refresh();
        }

        private void Stop()
        {
            // hack - da bi se izbegla kratka pauza pri podesavanju maksimalne vrednosi...
            int oldMax = progressBar1.Maximum;
            progressBar1.Maximum = 1000;
            progressBar1.Value = progressBar1.Maximum;
            progressBar1.Value = progressBar1.Maximum - 1;
            progressBar1.Value = progressBar1.Maximum;
            progressBar1.Maximum = oldMax;

            btnStart.Enabled = true;         
            totalTime.Enabled = true;
            timeLabel.Text = "Prošlo: " + (int)totalTime.Value + " sekundi...";
            toolStripStatusLabel1.Text = "Finished!";
            timer1.Enabled = false;
            finished = true;
         }

        private void Start()
        {
            startTime = DateTime.Now;
            btnStart.Enabled = false;
            totalTime.Enabled = false;
            timer1.Enabled = true;
            finished = false;

            toolStripStatusLabel1.Text = "Running...";
        }
    }
}
