﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _P4_02_Forms_Controls
{
    public partial class HitMeBeatMe : Form
    {
        int numButtons = 1;
        Random randgen = new Random();

        public HitMeBeatMe()
        {
            InitializeComponent();
        }

        private void HitMeBeatMe_Load(object sender, EventArgs e)
        {
            GenerateButton();
        }

        // Generisanje dugmeta "iz koda" i dodavanje kolekciji Controls na formi
        private void GenerateButton()
        {
            Button btn = new Button() { Height = button1.Height, Width = button1.Width };

            RandomlyMove(btn);
            
            btn.Text = "button" + (++numButtons);

            btn.Click += button1_Click;
            btn.MouseEnter += button1_MouseEnter;

            this.Controls.Add(btn);
        }

        // Event koji se aktivira kada se misem predje preko forme
        private void button1_MouseEnter(object sender, EventArgs e)
        {
            Button bSender = sender as Button;
            if (bSender == null) return;        // Za slucaj da je metoda pozvana sa strane
            RandomlyMove(bSender);
        }

        private void RandomlyMove(Button bSender)
        {
            // Nije moguce nezavisno menjanje X i Y koordinate, posto je Location property
            bSender.Location = new Point(
                randgen.Next(this.ClientSize.Width - bSender.Width),
                randgen.Next(this.ClientSize.Height - bSender.Height)
                );
        }
        // Ukljuciti KeyPreview property na formi da bi se aktivirali key eventi
        private void HitMeBeatMe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'b' || e.KeyChar == 'B')
            {
                GenerateButton();
            }
           
        }

        // Nakon promene velicine forme, pomeri svako dugme
        // promena
        private void HitMeBeatMe_ResizeEnd(object sender, EventArgs e)
        {
            foreach (Control ctrl in this.Controls)
                if (ctrl is Button)
                {
                    RandomlyMove(ctrl as Button);
                }
        }

        // pomeranjem misa na formi, fokus se prebacuje na skriveni textbox
        private void HitMeBeatMe_MouseMove(object sender, MouseEventArgs e)
        {
            textBox1.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
