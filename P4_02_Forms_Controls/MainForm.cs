﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace _P4_02_Forms_Controls
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.Text += " " + Properties.Resources.Autor;

            Thread thrdTime = new Thread(() =>
            {
                while (true)
                {
                    string time = DateTime.Now.ToLongTimeString();

                    // Pristup kontrolama iz drugog threada
                    this.Invoke(new Action(() =>
                    {
                        lblTime.Text = time;
                    }));

                    Thread.Sleep(1000);
                }
            });

            thrdTime.IsBackground = true;
            thrdTime.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Prikazivanje druge forme, pri cemu prva nastavlja sa radom
            GoJohnnyGo frm = new GoJohnnyGo();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            HitMeBeatMe frm = new HitMeBeatMe();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // Prikazivanje forme u obliku dialoga. Prva forma se zamrzava dok ova koja se prikazuje ne zavrsi sa radom...
            GoJohnnyGo frm = new GoJohnnyGo();
            frm.ShowDialog();

            // Moze da se updateuje i DialogResult forme (GoJohnnyGo) ali se prilikom dodele vrednosti ovog property-ja forma automatski zatvara
            // Zato je bolje da se parametri/rezultati prenose pomocu promenljivih i/ili property-ja
            if (frm.finished)
            {
                MessageBox.Show("Zavrsio", "Poruka", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // DialogResult dr = MessageBox.Show( ... );
                // if (dr == DialogResult.OK) ...
            }
            else
            {
                MessageBox.Show("Nije zavrsio, radio ukupno: " + frm.totalSec + " sekundi",
                    "Poruka", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ImageShow frm = new ImageShow();
            frm.Show();
        }
    }
}
