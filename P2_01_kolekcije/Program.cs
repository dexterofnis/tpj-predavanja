﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections; // da bi koristili kolekcije, moramo da ucitamo ovaj namespace

namespace _P2_01_kolekcije
{
    class Program
    {
        static void Main(string[] args)
        {
            // kolekcije
            CollectionArrayList();

            CollectionQueue();
            //vrlo slicno radi i Stack


            CollectionHashtable();
            //vrlo slicno radi i SortedList

            // initializeri
            ArrayList numbers = new ArrayList() { 10, 9, 8, 7, 7, 6, 5, 10, 4, 3, 2, 1 };
            Hashtable ages = new Hashtable() { { "John", 44 }, { "Diana", 45 }, { "James", 17 }, { "Francesca", 15 } };
            Hashtable ages1 = new Hashtable()
            {
                ["John"] = 44,
                ["Diana"] = 45,
                ["James"] = 17,
                ["Francesca"] = 15
            };

        }

        private static void CollectionArrayList()
        {
            ArrayList numbers = new ArrayList();
            ArrayList numbers1 = new ArrayList(10);
           
            for (int i = 0; i < 10; i++)
                numbers.Add(i * 10);
            numbers.Insert(numbers.Count - 1, 1000);
            numbers.RemoveAt(0); // indeksiranje pocinje od 0
            numbers.Remove(30);  // brise prvo pojavljivanje
            
            
            // prolaz kroz kolekciju
            for (int i = 0; i < numbers.Count; i++)
            {
                int num = (int)numbers[i];      // mora cast, zato sto ArrayList pamti objekte
                Console.Write($"{num} ");
            }
            Console.WriteLine();

            int broj = (int)numbers[2];

            // drugi nacin za prolaz kroz kolekciju
            foreach (int num in numbers)
                Console.Write($"{num} ");
            Console.WriteLine();
        }

        private static void CollectionQueue()
        {
            Queue numbers = new Queue();
            numbers.Enqueue(1); numbers.Enqueue(2); numbers.Enqueue(3);
            // empty the queue
            Console.Write("Izlaz iz reda : ");
            while (numbers.Count > 0)
            {
                int num = (int)numbers.Dequeue();
                Console.Write("{0} ", num);
            }
            Console.WriteLine("");
        }

        private static void CollectionHashtable()
        {
            Hashtable grades = new Hashtable();
            grades["Marko"] = 5;
            grades["Betty"] = 2;
            grades["Dacha"] = 3;
            grades["Aca"] = 5;
            grades["Mladen"] = 4;

            

            Console.WriteLine($"Betty: {grades["Betty"]}");

            foreach (DictionaryEntry element in grades)
            {
                string name = (string)element.Key;
                int grade = (int)element.Value;
                Console.WriteLine($"Ime: {name}, Ocena: {grade}");
            }
        }

    }
}
