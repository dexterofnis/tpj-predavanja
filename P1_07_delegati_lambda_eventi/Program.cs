﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_07_delegati_lambda_eventi
{

    class Program
    {
        static void Main(string[] args)
        {
            // delegati
            DelegatesAndLambda();

            // eventi
            Events();
        }


        delegate int MyFunct(int arg);      // delegat je prakticno funkcijski tip
        delegate int MyOtherFunct(ref int arg1, int arg2);
        delegate void Akcija();     // tip Action

        private static int FF1(int x) => x * x;
        private static void FF2(int x = 1) => Console.WriteLine(x);

        private static void DelegatesAndLambda()
        {
            MyFunct f0, f1, f2, f3;

            f0 = FF1;
            Console.WriteLine(f0(3));


            f1 = x => x * x;
            f2 = (int x) =>         // ne mora int da stoji
            {
                if (x % 2 == 0)
                    return x / 2;
                else
                    return 3 * x + 1;
            };

            f3 = f1;
            f1 = x => x * x * x;

            Console.WriteLine($"f1(3)={f1(3)} f3(3)={f3(3)}");

            PrintMyFunction(f1, f2);

            MyOtherFunct f4;
            // delegati mogu biti i komplikovaniji !!! 
            f4 = (ref int x, int y) => { x++; return x * y; };
            object f5 = f4;

            // delegati koji ne vracaju nikakvu vrednost
            Akcija a, a1;
            a = () => Console.WriteLine("Akcija1");
            a1 = () => Console.WriteLine("Akcija2");

            // ovim se duplira akcija2 u a i izvrsava se 2x
            a += a1; a += a1;
            a();

            // ako se elementi iz a1 ne sadrze u a, ne desava se nista
            a -= a1;
            a();

            // a += FF2; ne prepoznaje se defaultna vrednost
            a += () => FF2(4);
            
            a = null;
            //a();        // a.Invoke();

            a?.Invoke();    // if (a!=null) a.Invoke()

            // see also : Lambda Expressions (C# Programming Guide)
        }

        // parametri ove metode su funkcije i moze ih biti proizvoljno mnogo 
        private static void PrintMyFunction(params MyFunct[] list)
        {
            for (int i = 0; i < 9; i++)
            {
                Console.Write("{0,3:D}", i);

                foreach (MyFunct f in list)
                    Console.Write("{0,6:D}", f(i));

                Console.WriteLine();
            }
        }


        private static void Events()
        {
            Machine myMachine1 = new Machine("Machine1");
            Machine myMachine2 = new Machine("Machine2");
            Machine myMachine3 = new Machine("Machine3");

            myMachine1.ItemFinished += myMachine_ItemFinished;
            myMachine2.ItemFinished += myMachine_ItemFinished;
            myMachine3.ItemFinished += myMachine_ItemFinished;

            myMachine1.ItemFinished += (sender, itemNo) => { Console.WriteLine("Another event subscriber :)"); };

            myMachine1.DoIt(); myMachine1.DoIt(); myMachine1.DoIt();
            myMachine2.DoIt(); myMachine2.DoIt();
            myMachine3.DoIt();
        }

        private static void myMachine_ItemFinished(object sender, int itemNo)
        {
            Console.WriteLine((sender as Machine).Name + " finished item no. " + itemNo.ToString());
        }
    }
}
