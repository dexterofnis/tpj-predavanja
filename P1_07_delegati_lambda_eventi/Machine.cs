﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_07_delegati_lambda_eventi
{
    class Machine
    {
        int itemsFinished = 0;

        public delegate void ItemFinishedDelegate(object sender, int itemNo);
        public event ItemFinishedDelegate ItemFinished;

        public string Name
        { get; set; }

        public Machine(string name)
        {
            this.Name = name;
        }

        public void DoIt()
        {
            //Console.WriteLine("New item processing...");
            System.Threading.Thread.Sleep(1000);

            // ekvivalent ItemFinished(this, itemsFinished++)
            // ? znaci da se izvrsava samo ako je ItemFinished != null
            
            ItemFinished?.Invoke(this, itemsFinished++);
        }

    }
}
