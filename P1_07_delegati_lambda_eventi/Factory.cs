﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _05_property_indexeri_eventi
{

    class Folder
    {
        bool working = false;

        public void StartFolding()
        { working = true; Console.WriteLine("Folding started..."); }

        public void StopFolding(int stopTime)
        { working = false; Console.WriteLine("Folding stopped in {0} secs...", stopTime); }
    }

    class Welder
    {
        bool working = false;

        public void StartWelding()
        { working = true; Console.WriteLine("Welding started..."); }

        public void StopWelding()
        { working = false; Console.WriteLine("Welding stopped..."); }
    }

    class Painter
    {
        bool working = false;

        public void StartPainting()
        { working = true; Console.WriteLine("Painting started..."); }

        public void StopPainting()
        { working = false; Console.WriteLine("Painting stopped..."); }
    }


    class Factory
    {
        delegate void MachineryDelegate();
        private MachineryDelegate start = null, stop = null;

        Folder myFolder = new Folder();
        Welder myWelder = new Welder();
        Painter myPainter = new Painter();

        public Factory()
        {
            start += myFolder.StartFolding;
            start += myWelder.StartWelding;
            start += myPainter.StartPainting;

            stop = stop + (() => { myFolder.StopFolding(0); });
            stop += myWelder.StopWelding;
            stop += myPainter.StopPainting;

            // metoda se izbacuje pomocu
            // stop -= myPainter.StopPainting;
            // pritom, ako prethodno nije ni ubacena, ne javlja se greska
        }

        public void Start()
        { if (start != null) start(); }

        public void Stop()
        { if (stop != null) stop(); }
        
    }

    class TemperatureMonitor
    {
        public delegate void StopMachineryDelegate();
        public event StopMachineryDelegate MachineOverheating;

        public void SomethingHappened()
        {
            // ako se pozove event (delegat) koji je null, nastaje greska...
            // event, za razliku od obicnog delegata, ne moze da se pozove izvan klase
            if (MachineOverheating != null) MachineOverheating();
        }
    }
}
