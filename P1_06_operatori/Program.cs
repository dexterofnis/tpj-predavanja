﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace _10_operatori
{
    class Program
    {

        static void Main(string[] args)
        {
            Rational x = new Rational(1, -2), y = new Rational(1, 3);


            Console.WriteLine($"{-x * x + y} {x - y} {x * y} {x / y}");

            if (x == new Rational(1, 2))
                Console.WriteLine("x je jednako jednoj polovini");

            Console.WriteLine($"{x + 1} {1 - y} {3.1 * y} {(double)x} {(int)x}");

            double dx = (double)x;
            f(x);       // poziv sa implicitnom konverzijom Rational -> double
            f(dx);      // normalan poziv

            if ((x > -1) && (x < 1))
                Console.WriteLine("x je izmedju -1 i 1");

            Rational z = 5;
            Console.WriteLine(z);

            int rounded = (int)x;       // eksplicitna konverzija
            Console.WriteLine($"Original {x} and rounded {rounded}");
            //int rounded1 = x;        // ne moze jer ne postoji implicitna konverzija

            AAA a = 4;

        }

        static void f(double x) => Console.WriteLine($"Vrednost param. je: {x}");
        

    }

    // see also: Overloadable operators (C# Programming Guide)
}
