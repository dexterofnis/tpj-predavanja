﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _10_operatori
{

    struct Rational
    {

        #region PrivateMembers
        private int num, den;
       
        #endregion        
        
        // CTRL + K + X, pa onda Visual C#, pa #region
        #region Constructors

        public Rational(int a)
        {
            num = a; den = 1;
        }

        public Rational(int num, int den)
        {
            if (den == 0)
                throw new Exception("Denominator of the fraction must not be equal to zero");

            this.num = num; this.den = den;
            if (den < 0)
            {
                this.num = -this.num; this.den = -this.den;
            }
            Together();

        } 

        #endregion

        #region Private functions

        private static int GCD(int a, int b)
        {
            int r;

            if (a > b) { r = a; a = b; b = r; }

            while (b != 0)
            {
                r = a % b;
                a = b;
                b = r;
            }

            return a;
        }

        private void Together()
        {
            int gcd = GCD(Math.Abs(num), den);
            num /= gcd;
            den /= gcd;
        } 

        #endregion

        #region Arithmetic operators

        public static Rational operator +(Rational x, Rational y)
        {
            //Rational zbir;
            //zbir.num = ...
            //zbir.den = ...
            //return zbir;

            return new Rational(
                x.num * y.den + x.den * y.num,
                x.den * y.den
                );
        }

        public static Rational operator -(Rational x, Rational y)
        {
            return new Rational(
                x.num * y.den - x.den * y.num,
                x.den * y.den
                );
        }

        public static Rational operator *(Rational x, Rational y)
        {
            return new Rational(
                x.num * y.num,
                x.den * y.den
                );
        }

        public static Rational operator /(Rational x, Rational y)
        {
            return new Rational(
                x.num * y.den,
                x.den * y.num
                );
        } 

        public static Rational operator -(Rational x)
        {
                return new Rational(-x.num, x.den);
        }

        #endregion

        #region Logic operators

        // Kad se preklapa ==, trebalo bi da se preklopi i Equals metoda, kao i GetHashCode metoda
        public override bool Equals(object obj)
        {
            if (obj is Rational)
            {
                Rational another = (Rational)obj;
                return  (another.num == this.num) && 
                        (another.den == this.den);
            }
            else return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        // Operatori == i != idu u paru. Ako se napise (preklopi) jedan, mora (eksplicitno) da se preklopi i drugi!!!
        public static bool operator ==(Rational x, Rational y)
        {
            return x.Equals(y);
        }

        public static bool operator !=(Rational x, Rational y)
        {
            return !(x == y);
        }

        // Takodje, < i > idu u paru
        public static bool operator <(Rational x, Rational y)
        {
            return (x.num * y.den < y.num * x.den);
        }

        public static bool operator >(Rational x, Rational y)
        {
            //return (!(x < y) && (x != y));
            return y < x;
        }

        #endregion

        #region Conversion operators

        // Implicitna konverzija iz int u Rational 
        // (posle ovoga, int moze da se prosledjuje metodi (operatoru)
        // umesto parametra tipa Rational... 
        public static implicit operator Rational(int from)
        {
            return new Rational(from);
        }

        public static implicit operator float(Rational from)
        {
            Console.WriteLine($"{from} to float");

            return ((float)from.num) / from.den;
        }

        public static implicit operator double(Rational from)
        {
            return ((double)from.num) / from.den;
        }
        
        // Eksplicitna konverzija... npr iz Rational u int. 
        // Koristimo zaokruzivanje na najblizi ceo broj.
        public static explicit operator int(Rational from)
        {
            return (int)Math.Round((double)from);
        }

        // ne moze i jedan i drugi posebno
        //public static explicit operator Rational(int from)
        //{
        //    return new Rational(from);
        //}

        #endregion


        public override string ToString()
        {
            if (den == 1)
                return num.ToString();
            else
                return String.Format($"{num}/{den}");
        } 
    }

    public class AAA
    {

        int x;
        public AAA(int x)
        {
            this.x = x;

        }

        public static implicit operator AAA(int x)
        {
            return new AAA(x);
        }

    }

}
