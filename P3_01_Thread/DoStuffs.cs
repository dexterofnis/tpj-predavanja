﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using static System.Math;

namespace _P3_01_Thread
{
    partial class Program
    {
        static void Proc1()
        {
            Console.WriteLine("Proc1");
        }

        static void Proc2()
        {
            Console.WriteLine("Proc2");
        }

        static void ParamProc1(int m, ref int n)
        {
            Thread.Sleep(1000);
            n = m;
        }

        static void ParamProc2(object param)
        {
            Console.WriteLine($"Parametar je : {param}");
        }

        static void WriteDots()
        {
            while (true)
            {
                Console.Write(".");
                Thread.Sleep(100);
            }
        }

        static double MCIntegrate(int N)
        {
            Random rnd = new Random();
            double S = 0;

            for (int i = 0; i < N; i++)
            {
                double x = rnd.NextDouble();
                double y = rnd.NextDouble();
                double z = rnd.NextDouble();

                S += Exp(Sin(x * y * z));
            }

            return S / N;
        }

        static void Inc1(ref int v, int n)
        {
            for (int i=0; i<n; i++)
            {
                double something = Sin(Exp(Atan(PI / (v % 10))));
                v++;  
            }
        }

        static void Inc1Lock(ref int v, int n, object lockon)
        {
            for (int i = 0; i < n; i++)
            {
                lock (lockon)
                {
                    double something = Sin(Exp(Atan(PI / (v % 10))));
                    v++;
                }
            }
        }

        static void Inc1Interlocked(ref int v, int n)
        {
            for (int i = 0; i < n; i++)
            {
                double something = Sin(Exp(Atan(PI / (v % 10))));
                Interlocked.Increment(ref v);
            }
        }
    }
}
