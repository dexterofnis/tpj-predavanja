﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Diagnostics;
using System.IO;

namespace _P3_01_Thread
{
    partial class Program
    {
        static void Main(string[] args)
        {
            SimpleThreads();

            PassArg1();

            PassArg2();

            JoinAbort();

            SemaphoreSync();

            ThreadProperties();

            SyncInterlocked();

            Lock();

            StartExternalProcess();

            DirCommand();

        }

        private static void SimpleThreads()
        {
            Thread thrd1 = new Thread(Proc1);
            //Thread thrd1 = new Thread(new ThreadStart(DoStuff1));
            Thread thrd2 = new Thread(Proc2);

            // Startovanje oba threada
            thrd1.Start();
            thrd2.Start();

            Console.WriteLine("Glavni program");
        }

        private static void PassArg1()
        {
            int m = 0, n = 1;
            Thread thrd = new Thread(() => ParamProc1(m, ref n));
            thrd.Start();
            
            thrd.Join(); // Ceka dok se thrd ne zavrsi
            Console.WriteLine($"m={m}, n={n}");
        }

        private static void PassArg2()
        {
            //Thread thrd = new Thread(new ParameterizedThreadStart(ParamProc2));
            Thread thrd1 = new Thread(ParamProc2);
            Thread thrd2 = new Thread(ParamProc2);

            thrd1.Start(10);
            thrd2.Start("blablabla");
        }

        private static void JoinAbort()
        {
            double result = 0;

            Thread workthrd = new Thread(() => result = MCIntegrate(20000000));
            Thread dotsthrd = new Thread(WriteDots);

            workthrd.Start();
            dotsthrd.Start();

            workthrd.Join();

            dotsthrd.Suspend();     // privremeno zaustavi thread
            Thread.Sleep(1000);
            dotsthrd.Resume();      // nastavi sa radom
            Thread.Sleep(1000);
           
            dotsthrd.Abort();       // zaustavi u potpunosti

            Console.WriteLine();

            Console.WriteLine($"Result is : {result}");
        }

        private static void SemaphoreSync()
        {
            // Sinhronizacija strukturom Semafor (Semaphore):
            // Operacije su WaitOne() (dekrementiranje) i Release() (inkrementiranje) semaforske promenjive;
            // Kada vrednost sem. promenljive postane 0, thread se suspenduje dok neki drugi thread ne inkrementira promenljivu;
            
            Semaphore sem = new Semaphore(1, 2);    // pocetna i maksimalna vrednost promenljive

            Thread thrd1 = new Thread(
                () => 
                {
                    while(true)
                    {
                        sem.WaitOne();              // Omogucava da se thread zaustavi samo na ovom mestu
                        Console.Write('.');
                        Console.Write('-');
                        Thread.Sleep(100);
                        sem.Release();
                    }
                });

            thrd1.Start();                              
            Thread.Sleep(1000);
            sem.WaitOne();                              // Prekid nakon 1 sec rada
            Console.WriteLine("\n" + "Pause 1 sec");
            Thread.Sleep(1000);
            sem.Release();                              // Nastavak jos jednu sekundu
            Thread.Sleep(1000);
            sem.WaitOne();
            Console.WriteLine("\n" + "The end!!");      
            thrd1.Abort();                              // Prekid izvrsenja. Primetimo da thread ceka na tacno odredjenom mestu
        }

        private static void ThreadProperties()
        {
            // Thread koji se upravo izvrsava
            Thread currthrd = Thread.CurrentThread;

            Console.WriteLine("isAlive : {0}; isBackground : {1}",
                currthrd.IsAlive,       // da li se izvrsava
                currthrd.IsBackground   // da li radi u pozadini
                );

            // prioritet threada
            currthrd.Priority = ThreadPriority.Highest;
        }

        private static void SyncInterlocked()
        {
            int val = 0;
            
            Thread thrd1 = new Thread(() => Inc1(ref val, 100000));
            Thread thrd2 = new Thread(() => Inc1(ref val, 100000));
            thrd1.Start(); thrd2.Start();
            thrd1.Join(); thrd2.Join();

            Console.WriteLine($"Brojanje bez interlocked klase : {val}");

            val = 0;

            thrd1 = new Thread(() => Inc1Interlocked(ref val, 100000));
            thrd2 = new Thread(() => Inc1Interlocked(ref val, 100000));
            thrd1.Start(); thrd2.Start();
            thrd1.Join(); thrd2.Join();

            Console.WriteLine($"Brojanje sa interlocked klasom : {val}");
        }

        private static void Lock()
        {
            int val = 0, N = 1000000;

            object lockon = new object();

            Thread thrd1 = new Thread(() => Inc1Lock(ref val, N, lockon));
            Thread thrd2 = new Thread(() => Inc1Lock(ref val, N, lockon));

            thrd1.Start(); thrd2.Start();
            thrd1.Join(); thrd2.Join();

            Console.WriteLine(val);
        }

        private static void StartExternalProcess()
        {
            Process newProc = Process.Start("wordpad.exe");
            Console.WriteLine("New process started.");

            newProc.WaitForExit(10000);

            if (!newProc.HasExited)
            {
                newProc.Kill();
                Console.WriteLine("Process killed.");
            }
            else
                Console.WriteLine("New process ended.");

            newProc.Close(); // free resources
        }

        public static void DirCommand()
        {
            Process newProc = new Process();
            newProc.StartInfo.UseShellExecute = false;          // da li se startuje exe fajl ili poziva fajl nekog tipa (npr. doc)
            newProc.StartInfo.RedirectStandardInput = true;     
            newProc.StartInfo.RedirectStandardOutput = true;
            newProc.StartInfo.CreateNoWindow = true;            // da li se ne kreira prozor
            newProc.StartInfo.FileName = "cmd.exe";             // ime fajla
            newProc.Start();

            Thread.Sleep(5000);

            StreamWriter procIn = newProc.StandardInput;
            StreamReader procOut = newProc.StandardOutput;

            procIn.WriteLine("dir");
            procIn.WriteLine("exit");
            newProc.WaitForExit();

            do
            {
                Console.WriteLine(procOut.ReadLine());
            }
            while (!procOut.EndOfStream);

            newProc.Close();
        }
    }
}
