﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P1_02_klase
{
    struct Time
    {
        private int hours, minutes, seconds;
        
        // public Time() { hours = minutes = seconds = 0; } 
        // compile-time error, konstruktor ne sme biti bez parametara!!!

        // private int hours = 0;
        // compile-time error, promenljive se ne mogu inicijalizovati na pocetku
        
        public Time(int hh, int mm, int ss)
        {
            hours = hh % 24;
            minutes = mm % 60;
            seconds = ss % 60;
        }

        public int Hours()
        {
            return hours;
        }

        public void Tick()
        {
            seconds++;
            
            if (seconds == 60) { seconds = 0; minutes++; }

            if (minutes == 60) { minutes = 0; hours++; }

            if (hours == 24) hours = 0;
        }

        public override string ToString()
        {
            return String.Format("{0:D2}:{1:D2}:{2:D2}", hours, minutes, seconds);
        }

    }
}
