﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P1_02_klase
{
    static class Pass
    {

        public static void Value(int param)
        {
            param = 42;
        }

        public static void ValueRef(ref int param)
        {
            param = 42;
        }

        public static void ValueOut(out int param)
        {
            //param++; //-compiler error
            param = 42;
        }
    }
}
