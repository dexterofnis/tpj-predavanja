﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P1_02_klase
{
    class Circle
    {
        // privatna promenljiva
        private int radius;
        // promenljiva zajednicka za sve instance klase
        private static int numCircles = 0;

        public double Area()
        {
            return Math.PI * radius * radius;
        }

        public double ReturnRadiusOf(Circle circ)
        {
            // privatni clanovi se vide i za druge objekte iste klase
           return circ.radius;
        }

        // staticka metoda koja vraca vrednost privatne promenljive numCircles
        public static int GetNumCircles()
        {
            return numCircles;
        }

        // default konstruktor
        public Circle()
        {
            radius = 0;
            numCircles++;
        }

        // konstruktor
        public Circle(int radius)
        {
            this.radius = radius;
            numCircles++;
        }

        ~Circle()
        {
            Console.WriteLine("Pozvao se");
            numCircles--;
        }

        public override string ToString()
        {
            return "O " + base.ToString();
        }


    }
}
