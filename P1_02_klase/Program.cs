﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P1_02_klase
{
    class Program
    {
        enum Season { Spring, Summer = 5, Fall, Winter, Autumn = Fall }

        static void Main(string[] args)
        {
            Classes();
            
            // nullable types
            NullableTypes();

            // ref i out
            RefOut();

            UnsafeEnum();

            Structs();
        }

        private static void Classes()
        {
            // deklaracija promenljive (objekta) tipa Circle
            Circle c = null, d = null, e = new Circle();

            double areaOfCircle;

            // poziv default konstruktora
            c = new Circle();
            
            areaOfCircle = c.Area();
            Console.WriteLine("Area of the circle c is : {0}", areaOfCircle);
                       
            // poziv nedefault konstruktora
            d = new Circle(10);
            areaOfCircle = c.Area();
            Console.WriteLine("Area of the circle d is : {0}", areaOfCircle);

            Console.WriteLine(d.ToString());
            object dobj = d;
            Console.WriteLine(dobj.ToString());

            Console.WriteLine("{0}", Circle.GetNumCircles());

            // anonymous classes
            var myAnonymousObject = new { Name = "John", Age = 44 };
            Console.WriteLine("Name: {0} Age: {1}", myAnonymousObject.Name, myAnonymousObject.Age);

            // object tip i boxing
            object o = c;
            int i = 42;
            o = i;  // o postaje boxed int
            // i = o; // compiler error - o moze biti bilo sta, ne samo boxed int
            i = (int)o; // unboxing - ako o nije boxed int, daje gresku (InvalidCastException)
            
            // data casting
            o = c;  // o je Circle

            if (o is Circle)
                d = (Circle)o;
            else
                d = null;
            
            // d = o; - compiler error
            d = o as Circle;    // radi isto sto i prethodni kod
        }

        private static void NullableTypes()
        {
            int? i, k = null; // nisu u pitanju reference, vec samo podrska za null (uninitialized)
            double? a; 

            int j = 99;

            
            i = 100; // Copy a value type constant to a nullable type
            i = j; // Copy a value type variable to a nullable type
            // j = i; // Compiler error
            
            k = i;
            k = 2;  // nisu u pitanju reference, pa se ovim promenljiva i ne menja...
            
            // .HasValue property utvrdjuje da li ti 
            if (!i.HasValue)
                i = 99;
            else
                Console.WriteLine("Vrednost nullable promenljive i je {0}",i.Value);

            // see also : Nullable <T> Structure
        }

        private static void RefOut()
        {
            int i, j = 0;
            Pass.Value(j);          // nista se ne desava, prenos je po vrednosti...
            Console.WriteLine("j={0}", j);

            Pass.ValueRef(ref j);   // promenljiva mora biti inicijalizovana
            //Pass.Value(i); - compiler error!! 
            Pass.ValueOut(out i);   // moze i neinicijalizovana promenljiva
            
            Console.WriteLine("i={0}, j={1}", i, j);
        }

        private static void UnsafeEnum()
        {
            // unsafe code
            int x = 10, y = 20;
            unsafe
            {
                swap(&x, &y);
            }
            Console.WriteLine("Unsafe code gives : (x,y)=({0},{1})", x, y);


            // enum tip
            Season mySeason;
            Season? mySeasonNullable = null;

            mySeason = Season.Fall;
            Console.WriteLine("My season is : {0}", mySeason);
            Console.WriteLine("My season converted to int is : {0}", (int)mySeason);

            // enum je zapravo int. Ako je npr Fall = 2 a Winter = 4, sledeci kod ispisuje 3.
            // by default, vrednosti tipa enum odgovaraju uzastopnim int-ovima...
            // mySeason++;
            Console.WriteLine("My next season is : {0}", ++mySeason);
        }

        public static unsafe void swap(int* a, int* b)
        {
            int temp;
            temp = *a;
            *a = *b;
            *b = temp;
        }

        private static void Structs()
        {
            // struct
            Time t2 = new Time();
            Time t1 = new Time(2, 43, 44);
            Time copy = t1;
            Time t;

            Time myTime = new Time(1, 2, 3);
            Increase10s(ref myTime);

            t1.Tick();
            Console.WriteLine(t1.Hours());
            Console.WriteLine(t1);
        }

        private static void Increase10s(ref Time myTime)
        {
            for (int i = 0; i < 10; i++)
                myTime.Tick();
        }
    }
}
