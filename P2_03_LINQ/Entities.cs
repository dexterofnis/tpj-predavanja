﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P2_03_LINQ
{
    class Customer : IComparable<Customer>
    {
        private static int _id = 0;

        public Customer()
        {
            CustomerID = _id++;
        }

        public int CustomerID { get; private set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }

        public override string ToString()
        {
            return String.Format("ID: {0}: Customer {1} {2} from {3}", CustomerID, FirstName, LastName, CompanyName);
        }

        int IComparable<Customer>.CompareTo(Customer other)
        {
            return (other == null) ? 1 : 
                this.CustomerID.CompareTo(other.CustomerID);
        }

    }

    class Address
    {
        private static int _id = 0;
        public Address()
        {
            AddressID = _id++;
        }

        public int AddressID { get; private set; }
        public string CompanyName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public override string ToString()
        {
            return String.Format("Company {0} from {1}, {2}", CompanyName, City, Country);
        }
    }
 
}
