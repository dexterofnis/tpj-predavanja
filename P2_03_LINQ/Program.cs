﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BinarnoStablo;

namespace _P2_03_LINQ
{
   
    static class Program
    {

        // data
        #region Data
        static Customer[] customers = new Customer[] {
                new Customer() { FirstName = "Orlando", LastName = "Gee", CompanyName = "A Bike Store" },
                new Customer() { FirstName = "Keith", LastName = "Harris", CompanyName = "Bike World" },
                new Customer() { FirstName = "Donna", LastName = "Carreras", CompanyName = "A Bike Store" },
                new Customer() { FirstName = "Janet", LastName = "Gates", CompanyName = "Fitness Hotel" },
                new Customer() { FirstName = "Lucy", LastName = "Harrington", CompanyName = "Grand Industries" },
                new Customer() { FirstName = "David", LastName = "Liu", CompanyName = "Bike World" },
                new Customer() { FirstName = "Donald", LastName = "Blanton", CompanyName = "Grand Industries" },
                new Customer() { FirstName = "Jackie", LastName = "Blackwell", CompanyName = "Fitness Hotel" },
                new Customer() { FirstName = "Elsa", LastName = "Leavitt", CompanyName = "Grand Industries" },
                new Customer() { FirstName = "Eric", LastName = "Lang", CompanyName = "Distant Inn" }
            };


        static Address[] addresses = new Address[] {
                new Address() { CompanyName = "A Bike Store", City = "New York", Country = "United States"},
                new Address() { CompanyName = "Bike World", City = "Chicago", Country = "United States"},
                new Address() { CompanyName = "Fitness Hotel", City = "Ottawa", Country = "Canada"},
                new Address() { CompanyName = "Grand Industries", City = "London", Country = "United Kingdom"},
                new Address() { CompanyName = "Distant Inn", City = "Tetbury", Country = "United Kingdom"}
            };
        #endregion

        static void Main(string[] args)
        {

            // sve metode nalaze se u statickoj klasi Enumerable (System.LINQ) 
            // i prosiruju IEnumerable interface
            
           
            SelectMetoda();

            WhereMetoda();

            OrderByCountMinMax();

            GroupByMetoda();

            DistinctMetoda();

            JoinMetoda();

            DeferredExecution();

            Stablo();

            JosPrimera();

            // see also : Enumerable Class, System.Linq Namespace
        }
        
        private static void PrintList<T>(this IEnumerable<T> list)
        {
            Console.WriteLine("-------------");
            foreach (T item in list)
                Console.WriteLine(item);
            Console.WriteLine("-------------\n");
        }

        private static List<T> RandomPermute<T>(this List<T> x)
        {
            int n = x.Count;
            List<int> ind = new List<int>(n);
            List<T> newList = new List<T>();
            Random rnd = new Random();

            for (int i = 0; i < n; i++)
                ind.Add(i);

            for (int i=0; i<n; i++)
            {
                int curr = rnd.Next(ind.Count-1);
                T elem = x[ind[curr]];
                newList.Add(elem);
                ind.Remove(ind[curr]);
            }

            return newList;
        }

        private static void SelectMetoda()
        {
            Func<int, int> d = (x => x ^ 2);
            
            // select metoda ima parametar koji je funkcija (delegat) i vraca kolekciju vrednosti ove funkcije \
            // primenjene na elemente polazne kolekcije
            IEnumerable<string> customerFirstNames = customers.Select(cust => cust.FirstName);

            //List<string> cFN = new List<string>();
            //foreach (Customer cust in customers)
            //    cFN.Add(cust.FirstName);

            // drugi nacin, pomocu query operatora
            customerFirstNames = from cust in customers
                                 select cust.FirstName;
            customerFirstNames.PrintList(); // Automatski prepoznaje tip string

            // rezultat moze biti kolekcija anonimnih klasa (da ne bi definisali novu klasu samo za ovu potrebu). U tom slucaju, promenljiva se deklarise sa var
            var customerName = customers.Select(cust => new { FirstName = cust.FirstName, LastName = cust.LastName });
            
            // drugi nacin, pomocu query operatora
            customerName = from cust in customers
                           select new { FirstName = cust.FirstName, LastName = cust.LastName };

            customerName.PrintList();
        }

        private static void WhereMetoda()
        {
            // Where metoda filtrira elemente kolekcije koji zadovoljavaju uslov. 
            // Parametar je funkcija (delegat) koja za svaki element polazne kolekcije vraca true ili false
            IEnumerable<string> usCompaniesNames =
                addresses.Where(addr => addr.Country == "United States")
                .Select(usComp => usComp.CompanyName);

            // usCompanies = Enumerable.Where<Address>(addresses, addr => addr.Country == "United States")
            //                         .Select(usComp => usComp.CompanyName);

            // drugi nacin, pomocu query operatora
            usCompaniesNames = from a in addresses
                               where a.Country == "United States"
                               select a.CompanyName;

            usCompaniesNames.PrintList();
        }

        private static void OrderByCountMinMax()
        {
            // OrderBy sortira polja po vrednosti funkcije koja je parametar metode
            IEnumerable<string> companyNames =
                addresses.OrderBy(addr => addr.CompanyName)
                .ThenBy(addr => addr.City)
                .Select(comp => comp.CompanyName);

            // drugi nacin, pomocu query operatora
            companyNames = from a in addresses
                           orderby a.CompanyName ascending, a.City descending
                           select a.CompanyName;

            companyNames.PrintList();


            // select * u SQL-u
            var myQuery = from a in addresses
                          where a.City == "Nis"
                          select a;
            myQuery = addresses.Where(a => (a.City == "Nis"));

            // Metode Count, Min, Max, Sum, ...
            Console.WriteLine("{0} | {1} | {2} | {3}",
                companyNames.Count(),
                companyNames.Count(name => name.EndsWith("Store")),
                companyNames.Max(),
                companyNames.Min(name => name.Length)
            );

            // drugi nacin za Count(...)
            int n = (from cn in companyNames
                     where cn.EndsWith("Store")
                     select cn).Count();

            // drugi nacin, pomocu query operatora
            int numberOfCompanies = (from a in addresses
                                     select a.CompanyName).Count();
        }

        private static void GroupByMetoda()
        {
            // GroupBy grupise elemente kolekcije koji imaju istu vrednost funkcije koja je parametar metode
            // vraca kolekciju ciji su elementi kolekcije koji sadrze (grupisu) elemente sa istom vrednoscu fje
            // property Key vraca (zajednicku) vrednost funkcije za grupu, Count br. elemenata grupe
            var companiesGroupedByCountry = addresses.GroupBy(addrs => addrs.Country);

            // drugi nacin, pomocu query operatora
            companiesGroupedByCountry = from a in addresses
                                        group a by a.Country;
            
                            

            // mesto var treba da stoji IGrouping<string,Address>
            foreach (var companiesPerCountry in companiesGroupedByCountry)
            {
                Console.WriteLine("Country: {0}\t{1} companies",
                companiesPerCountry.Key, companiesPerCountry.Count());
                companiesPerCountry.PrintList();
            }
            Console.WriteLine("-------");
        }

        private static void DistinctMetoda()
        {
            // Metoda Distinct izbacuje duplikate
            int numberOfCountries =
                addresses.Select(addr => addr.Country)
                .Distinct()
                .Count();

            // drugi nacin, pomocu query operatora
            numberOfCountries = (from a in addresses
                                 select a.Country).Distinct().Count();

            Console.WriteLine("Number of countries: {0}", numberOfCountries);
        }

        private static void JoinMetoda()
        {
            // Metoda Join sluzi za spajanje dve kolekcije
            // parametri su redom: druga kolekcija, funkcije elementa prve i druge kolekcije po kojima 
            // se vrsi spajanje, rezultujuca funkcija (elementa obe kolekcije) na osnovu koje se generise
            // rezultujuca kolekcija
            var citiesAndCustomers = customers
                .Join(addresses, custs => custs.CompanyName, addrs => addrs.CompanyName,
                    (custs, addrs) => new { custs.FirstName, custs.LastName, addrs.Country });

            // drugi nacin, pomocu query operatora
            citiesAndCustomers = from a in addresses
                                 join c in customers
                                 on a.CompanyName equals c.CompanyName
                                 select new { c.FirstName, c.LastName, a.Country };
           
            foreach (var row in citiesAndCustomers)
                Console.WriteLine(row);
            Console.WriteLine("-------");
        }

        private static void DeferredExecution()
        {
            // LINQ query se uvek izvrsava nad najnovijom (trenutnom) verzijom kolekcije (deffered evaluation).
            // Da bi se to sprecilo, stavlja se npr. customersList.ToList(), pa se query povezuje sa kopijom liste.
            List<Customer> customersList = new List<Customer>(customers);
            var jackies = from cust in customersList
                          where cust.FirstName == "Jackie"
                          select cust; //.ToList();

            // Ubacujemo novog customera nakon definicije query-ja
            Customer jc = new Customer() { FirstName = "Jackie", LastName = "Chan", CompanyName = "Kung-fu school" };
            customersList.Add(jc);

            // Stampa se i Jackie Chan
            jackies.PrintList();

            // Vise nema Jackie Chan-a
            customersList.Remove(jc);
            jackies.PrintList();
        }

        private static void Stablo()
        {
            // Izmesaj customere
            Customer[] customers1 = customers.ToList().RandomPermute().ToArray();

            // Ubaci ih u binarno stablo
            BinarnoStablo<Customer> custBinStablo = new BinarnoStablo<Customer>(customers1[0]);
            for (int i = 1; i < customers1.Length; i++)
                custBinStablo.Insert(customers1[i]);

            // Stampaj sve cija kompanija sadrzi Bike
            // Stampanje ide uvek u istom redosledu (po rastucem ID-ju) zato sto se izvlace
            // iz binarnog stabla
            (from cust in custBinStablo
             where cust.CompanyName.Contains("Bike")
             select cust).PrintList();
        }

        private static void JosPrimera()
        {
            char[] chr1 = new char[] { 'A', 'B', 'C' };
            char[] chr2 = new char[] { '1', '2', '3' };
            
            
            // ugnjezdeni from daje sve kombinacije (dekartov proizvod)
            var charPairs = from ch1 in chr1
                            from ch2 in chr2
                            select new { Prvi = ch1, Drugi = ch2 };

            foreach (var pair in charPairs)
                Console.WriteLine(pair.Prvi + " " + pair.Drugi);
            Console.WriteLine();

            // into naredba pamti trenutni rezultat query-ja u promenljivu
            // U ovom primeru, grupisemo web sajtove po tipu domena (.com, .net, .org, itd.) 
            // i biramo samo one grupe koje sadrze bar 2 elementa. 
            
            string[] websites = {   "hit.me",  "hsNameA.com", "hsNameB.net", "hsNameC.net",
                                    "hsNameD.com", "hsNameE.org", "hsNameF.org",
                                    "hsNameG.tv", "hsNameH.net", "hsNameI.tv", "sajtNemaTopLevel"   };
            var webAddrs =  from addr in websites
                            where addr.LastIndexOf('.') != -1
                            group addr by addr.Substring(addr.LastIndexOf('.'))
                                into ws
                            where ws.Count() >= 2
                            select ws;
           
            Console.WriteLine("Tipovi domena sa bar 2 člana:\n");
            foreach (var sites in webAddrs)
            {
                Console.WriteLine("Sajtovi u " + sites.Key + " domenu:");
                foreach (var site in sites)
                    Console.WriteLine(" " + site);
                Console.WriteLine();
            }

            
            // let naredba se koristi da se oznaci rezultat primene neke operacije na 
            // na promenljive iz query-ja, koji kasnije moze da se iskoristi u nastavku query-ja.
            

            // U ovom query-ju izvlacimo sve karaktere svih stringova u nizu strs i prikazujemo ih
            // rastucem redosledu
            string[] strs = { "alpha", "beta", "gamma" };
            var chrs = from str in strs
                       let chrArray = str.ToCharArray()
                       from ch in chrArray
                       orderby ch ascending
                       select ch;

            Console.WriteLine("Karakteri stringova (sortirani od najmanjeg ka najvećem):");
            foreach (char c in chrs) Console.Write(c + " ");
            Console.WriteLine();

        }

    }
}
