﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Microsoft.Win32;
using System.IO;
using System.Xml.Serialization;

namespace _P4_04_WPF_StudServis
{
    /// <summary>
    /// Interaction logic for StudServis.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        StudServisEntry sse = new StudServisEntry()
        {
            Ime = "Marko",
            Prezime = "Markovic",
            Godine = new List<Godina>()
            {
                new Godina()
                {
                    Naziv ="1. godina",
                    Predmeti = new List<Predmet>()
                    {
                        new Predmet() {Naziv="Analiza 1", Polozio=true },
                        new Predmet() {Naziv="Analiza 2", Polozio=false },
                        new Predmet() {Naziv="Uvod u programiranje", Polozio=true }
                    }
                },
                new Godina()
                {
                    Naziv ="2. godina",
                    Predmeti = new List<Predmet>()
                    {
                        new Predmet() {Naziv="Linearna algebra", Polozio=false },
                        new Predmet() {Naziv="Verovatnoca", Polozio=true }
                    }
                },
                new Godina()
                {
                    Naziv ="1. godina (master)",
                    Predmeti = new List<Predmet>()
                    {
                        new Predmet() {Naziv="TPJ", Polozio=false },
                        new Predmet() {Naziv="TIK", Polozio=true }
                    }
                }
            }
        };



        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = sse;

            // Podesavanje stila iz koda
            //lbPredmeti.Style = this.FindResource("redListBox") as Style;
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            string message = String.Format(
                "Student {0} {1} \n Finansiranje {2} \n Datum upisa {3} \n Ponovo u istu godinu {4} \n Polozeni predmeti:", sse.Ime, sse.Prezime, sse.Finansiranje, sse.DatumUpisa, sse.IstaGod);

            foreach (Godina god in sse.Godine)
                foreach (Predmet pred in god.Predmeti)
                    if (pred.Polozio)
                        message += String.Format("    {0}  ({1})\n", pred.Naziv, god.Naziv);

            MessageBox.Show(message);
        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            sse.Ime = sse.Prezime = "";
            sse.IstaGod = false;
            sse.Finansiranje = Finansiranje.IzBudzeta;
            sse.DatumUpisa = DateTime.Now;

            foreach (Godina god in sse.Godine)
                foreach (Predmet pred in god.Predmeti)
                    pred.Polozio = false;
        }

        private void cmbGodinaStudija_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lbPredmeti.Items.Clear();

            Godina god = cmbGodinaStudija.SelectedItem as Godina;
            if (god == null) return;

            foreach (Predmet pred in god.Predmeti)
            {
                CheckBox cb = new CheckBox();
                
                cb.DataContext = pred;
                cb.SetBinding(CheckBox.IsCheckedProperty,
                    new Binding("Polozio"));
                cb.SetBinding(CheckBox.ContentProperty, 
                    new Binding("Naziv"));
                lbPredmeti.Items.Add(cb);
            }
        }

        private void NoviStudent_Click(object sender, RoutedEventArgs e)
        {

        }

        private void UcitajStudenta_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.ValidateNames = true;
            openFile.Title = "Ucitaj studenta";
            openFile.Filter = "Text file|*.txt";

            if (openFile.ShowDialog() == true)
            {
                using (StreamReader sw = new StreamReader(openFile.FileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(StudServisEntry));

                    try
                    {
                        sse = serializer.Deserialize(sw) as StudServisEntry;
                        this.DataContext = sse;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error occured: \n " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void ZapamtiStudenta_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();

            //saveFile.InitialDirectory = @"C:\";
            saveFile.OverwritePrompt = true;
            saveFile.ValidateNames = true;
            saveFile.Title = "Zapamti studenta";
            saveFile.Filter = "Text file|*.txt";


            if (saveFile.ShowDialog().Value)
            {
                using (StreamWriter sw = new StreamWriter(saveFile.FileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(StudServisEntry));

                    try
                    {
                        serializer.Serialize(sw, sse);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error occured: \n " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void Izlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            About aboutWindow = new About();
            aboutWindow.ShowDialog();                
        }
    }
}
