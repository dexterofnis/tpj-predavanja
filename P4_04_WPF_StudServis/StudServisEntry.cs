﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace _P4_04_WPF_StudServis
{
    public enum Finansiranje { IzBudzeta, Samofinansiranje, Sufinansiranje }

    public class NotifyPropertyChanged : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        // Atribut [CallerMemberName] omogucava da default vrednost opcionog parametra propertyName 
        // bude naziv clanice klase (metode, property-ja) koja je pozvala OnPropertyChanged metod.
        // Tako se izbegava eksplicitno pisanje naziva propertyja prilikom poziva OnPropertyChanged metoda.

        // Postoje i [CallerFilePath] i [CallerLineNumber] atributi (pogledati Programming C# 5.0 - Ian Griffiths)
        protected void OnPropertyChanged(
             [CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Predmet : NotifyPropertyChanged
    {
        private string _naziv;
        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; OnPropertyChanged(); } // Ne mora da se navodi ime property-ja
        }

        private bool _polozio;
        public bool Polozio
        {
            get { return _polozio; }
            set { _polozio = value; OnPropertyChanged(); }
        }
    }
    
    public class Godina : NotifyPropertyChanged
    {
        private string _naziv;
        public string Naziv
        {
            get { return _naziv; }
            set { _naziv = value; OnPropertyChanged(); }
        }

        public List<Predmet> Predmeti { get; set; }

    }

    public class StudServisEntry : NotifyPropertyChanged
    {
        private string _ime;
        public string Ime
        {
            get { return _ime; }
            set { _ime = value;  OnPropertyChanged(); }
        }

        private string _prezime;
        public string Prezime
        {
            get { return _prezime; }
            set { _prezime = value; OnPropertyChanged(); }
        }

        private int _godinastudiranja;
        public int GodinaStudiranja
        {
            get { return _godinastudiranja; }
            set { _godinastudiranja = value; OnPropertyChanged(); }
        }

        private bool _istagod;
        public bool IstaGod
        {
            get { return _istagod; }
            set { _istagod = value;  OnPropertyChanged(); }
        }

        private Finansiranje _izbudzeta;
        public Finansiranje Finansiranje
        {
            get { return _izbudzeta; }
            set { _izbudzeta = value; OnPropertyChanged(); }
        }

        public DateTime _datumUpisa;
        public DateTime DatumUpisa
        {
            get { return _datumUpisa; }
            set { _datumUpisa = value; OnPropertyChanged(); }
        }

        public List<Godina> Godine { get; set; }

        public StudServisEntry()
        {
            DatumUpisa = DateTime.Now;
        }
        

    }
}
