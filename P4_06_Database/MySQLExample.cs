﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace _P4_06_Database
{
    class MySQLExample
    {
        public static void Run()
        {
            // remotemysql - dobra varijanta ako zelite da izbegnete ubacivanje baze na lokalni server
            string connStr = "server=localhost; user=user; database=smallbase; password=user";
            MySqlConnection msq = new MySqlConnection(connStr);

            try
            {
                Console.WriteLine("Connecting to MySQL database...\n\n");

                msq.Open();

                string strSelectCmd =
                    @" SELECT customer.Name, product.name, customerproduct.Quantity 
FROM customerproduct
left join product on (ProductId = product.ID)
left join customer on (CustomerId = customer.ID);";

                MySqlCommand selectCmd = new MySqlCommand(strSelectCmd, msq);
                MySqlDataReader rdr = selectCmd.ExecuteReader();

                // Ispisati string na odredjeni broj karaktera...
                Console.WriteLine($"{"Customer",10} | {"Product",25} | {"Quantity",10}");

                while (rdr.Read())
                {
                    Console.WriteLine($"{rdr.GetString(0),10} | {rdr.GetString(1),25} | {rdr.GetInt32(2),10}");
                }

                rdr.Close();

                Console.WriteLine();

                string strInsertCmd = "INSERT INTO customer (Name) VALUES ('Marko')";
                MySqlCommand insertCmd = new MySqlCommand(strInsertCmd, msq);
                int rowsAffected = insertCmd.ExecuteNonQuery();
                Console.WriteLine($"Insert command: {rowsAffected} rows affected");

                string strUpdateCmd = "UPDATE customer SET Name='Mare' WHERE Name='Marko'";
                MySqlCommand updateCmd = new MySqlCommand(strUpdateCmd, msq);
                rowsAffected = updateCmd.ExecuteNonQuery();
                Console.WriteLine($"Update command: {rowsAffected} rows affected");

                string strDeleteCmd = "DELETE FROM customer WHERE Name='Mare'";
                MySqlCommand deleteCmd = new MySqlCommand(strDeleteCmd, msq);
                rowsAffected = deleteCmd.ExecuteNonQuery();
                Console.WriteLine($"Delete command: {rowsAffected} rows affected");

                msq.Close();

                Console.WriteLine("\n\nFinished.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                msq.Close();
            }


        }

    }
}
