﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;


namespace _P4_06_Database
{
    class SQLServerExample
    {
        public static void Run()
        {
            // osnovna klasa, obavlja konekciju sa bazom
            SqlConnection conn = new SqlConnection();

            // klasa za formiranje connection stringa, za povezivanje sa bazom
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = ".\\SQLExpress";
            builder.InitialCatalog = "Northwind";
            builder.IntegratedSecurity = true;



            // ubacujemo connection string u connection objekat
            conn.ConnectionString = builder.ConnectionString;

            // A moze i ovako (eksplictino formiranje stringa):
            //string username = "John";
            //string password = "Doe";
            //string connString = String.Format(
            //    "User ID={0};Password={1};Initial Catalog=Northwind;" +
            //    "Data Source=YourComputer\\SQLExpress", username, password);

            try
            {
                Console.WriteLine("Connecting to SQL Server database...\n\n");

                // otvara konekciju ka SQL serveru...
                conn.Open();

                // definise SQL komandu
                string strSelectCmd = "SELECT OrderID, OrderDate, ShippedDate, ShipName, ShipAddress, " +
                    "ShipCity, ShipCountry " +
                    "FROM Orders WHERE CustomerID = @CustomerIdParam";
                
                SqlCommand selectCmd = new SqlCommand(strSelectCmd, conn);

                // string commandText = String.Format("... CId = {0}", textBox1.Text);

                // definise i dodaje odgovarajuci parametar komande
                SqlParameter param = new SqlParameter("@CustomerIdParam", SqlDbType.Char);

                string customerId = "BONAP";
                param.Value = customerId;
                selectCmd.Parameters.Add(param);

                Console.WriteLine($"About to find orders for customer {customerId}\n\n");

                // izvrsavanje komande i formiranje dataReader klase koja sadrzi rezultat
                SqlDataReader dataReader = selectCmd.ExecuteReader();


                // prolazak kroz svaki red rezultata
                while (dataReader.Read())
                {
                    // fje GetXXX izvlace vrednost odgovarajuce kolone
                    // parametar je redni br. kolone
                    int orderId = dataReader.GetInt32(0);
                    // ako je na tom polju null, fja GetXXX podize exception
                    if (dataReader.IsDBNull(2))
                    {
                        Console.WriteLine($"Order {orderId} not yet shipped\n\n");
                    }
                    else
                    {
                        DateTime orderDate = dataReader.GetDateTime(1);
                        DateTime shipDate = dataReader.GetDateTime(2);
                        string shipName = dataReader.GetString(3);
                        string shipAddress = dataReader.GetString(4);
                        string shipCity = dataReader.GetString(5);
                        string shipCountry = dataReader.GetString(6);
                        Console.WriteLine(
                            $"Order: {orderId}\nPlaced: {orderDate}\nShipped: {shipDate}\nTo Address: {shipName}\n{shipAddress}\n{shipCity}\n{shipCountry}\n\n");
                    }
                }
                dataReader.Close();

                //string queryString = "INSERT INTO Customers " + "(CustomerID, CompanyName) Values('NWIND', 'Northwind Traders')";

                string strInsertCmd = "INSERT INTO Products " + "(ProductName, SupplierID, UnitPrice) Values ('Magla', 1, 10)";
                SqlCommand insertCmd = new SqlCommand(strInsertCmd, conn);
                int rowsAffected = insertCmd.ExecuteNonQuery();
                Console.WriteLine($"Insert command: {rowsAffected} rows affected");

                string strUpdateCmd = "UPDATE Products SET ProductName = 'Debela magla' WHERE ProductName = 'Magla'";
                SqlCommand updateCmd = new SqlCommand(strUpdateCmd, conn);
                rowsAffected = updateCmd.ExecuteNonQuery();
                Console.WriteLine($"Update command: {rowsAffected} rows affected");

                string strDeleteCmd = "DELETE Products WHERE ProductName = 'Debela magla'";
                SqlCommand deleteCmd = new SqlCommand(strDeleteCmd, conn);
                rowsAffected = deleteCmd.ExecuteNonQuery();
                Console.WriteLine($"Delete command: {rowsAffected} rows affected");
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
