﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;

using MySql.Data.MySqlClient;



namespace _P4_06_Database
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLServerExample.Run();

            MySQLExample.Run();

            SQLiteExample.Run();
        }

    }
}
