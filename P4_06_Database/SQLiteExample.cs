﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SQLite;

namespace _P4_06_Database
{
    class SQLiteExample
    {
        public static void Run()
        {
            // Dodati fajl sa bazom u projekat i podesiti "Copy to Output directory" u Properties
            SQLiteConnection myconn = new SQLiteConnection("Data Source=SmallBase.db");

            try
            {
                Console.WriteLine("Connecting to SQLite database...\n\n");

                myconn.Open();

                string selcom = "SELECT * FROM Product";
                SQLiteCommand comm = new SQLiteCommand(selcom, myconn);

                SQLiteDataReader reader = comm.ExecuteReader();

                while (reader.Read())
                    Console.WriteLine("ID: {0}  Name: {1} UnitPrice: {2} ",
                        reader["ID"],
                        reader["Name"],
                        reader["SerialNo"]);

                reader.Close();

                Console.WriteLine();

                string insertcomm = "INSERT INTO Product (Name, SerialNo) VALUES ('Magla',10)";
                SQLiteCommand commInsert = new SQLiteCommand(insertcomm, myconn);
                int rowsAffected = commInsert.ExecuteNonQuery();
                Console.WriteLine($"Insert command: {rowsAffected} rows affected");

                string deletecomm = "DELETE FROM Product WHERE Name='Magla'";
                SQLiteCommand commDelete = new SQLiteCommand(deletecomm, myconn);
                rowsAffected = commDelete.ExecuteNonQuery();
                Console.WriteLine($"Delete command: {rowsAffected} rows affected");

                Console.WriteLine("\n\nFinished.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                myconn.Close();
            }

        }

    }
}
