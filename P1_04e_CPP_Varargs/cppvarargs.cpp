#include <stdio.h>
#include <stdarg.h>						// biblioteka koja sadrzi funkcije za rad sa opcionim parametrima

int maxof(int n_args, ...)
{
    va_list ap;							// pointer na trenutni opcioni parametar			
    va_start(ap, n_args);				// funkcija koja se poziva pre pristupa opcionim parametrima
										// drugi parametar predstavlja (neopcioni) parametar funkcije (maxof) koji prethodi opcionim parametrima
										// postoji varijanta i bez drugog parametra ali ona nije obuhvacena ANSI standardom
    int max = va_arg(ap, int);
    for (int i = 2; i <= n_args; i++) 
	{
        int a = va_arg(ap, int);		// vraca vrednost parametra kastovanu u zadati tip
        if(a > max) max = a;			
    }
    va_end(ap);							// poziva se na kraju funkcije, odnosno nakon sto je zavrsi pristup opcionim parametrima
    return max;
}

int main()
{
	printf("%d %d %d\n", maxof(3,1,2,3), maxof(2,1,2), maxof(1,1));
	
	return 0;
}