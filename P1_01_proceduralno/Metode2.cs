﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _P1_01_proceduralno
{
    partial class Program
    {

        static void Ispis(int myInt, double myDouble)
        {
            Console.Write("\n\n");

            Console.WriteLine("Default: {0} pa ponovo {0} i {1}", myInt, myDouble);

            Console.WriteLine("Default " + myInt + " " + myDouble);

            Console.WriteLine(
                "(C) Currency: . . . . . . . . {0:C}\n" +
                "(D) Decimal:. . . . . . . . . {0:D}\n" +
                "(E) Scientific: . . . . . . . {1:E}\n" +
                "(F) Fixed point:. . . . . . . {1:F}\n" +
                "(G) General:. . . . . . . . . {0:G}\n" +
                "    (default):. . . . . . . . {0} (default = 'G')\n" +
                "(N) Number: . . . . . . . . . {0:N}\n" +
                "(P) Percent:. . . . . . . . . {1:P}\n" +
                "(R) Round-trip: . . . . . . . {1:R}\n" +
                "(X) Hexadecimal:. . . . . . . {0:X}\n" +
                "    (default):. . . . . . . . {1} \n",
                myInt, myDouble
            );

            Console.WriteLine($"Default: {myInt} pa ponovo {myInt} i {myDouble}");

            Console.Write("\n\n");

            Console.WriteLine(
                "Decimal with 8 digits: {0:D8}\n" +
                "Scientific with small e and 10 decimals: {1:e10}\n" +
                "Fixed point with 5 decimals: {1:f5}\n",
                myInt, myDouble
            );

            Console.WriteLine(
                "Integer with alignment: {0,10:D}", myInt);

            string FormatPrice = String.Format("Price = |{0,10:C}|", myInt);

            // see also: Composite Formatting, Standard Numeric Format
        }

        static double Ucitavanje(ref int myInt, ref double myDouble)
        {
            double result = double.NaN;
            try
            {
                myInt = int.Parse(Console.ReadLine());
                myDouble = double.Parse(Console.ReadLine());

                result = myInt * myDouble;
            }
            catch (FormatException fEx)
            {
                Console.WriteLine(fEx.Message);
                result = -1;
            }
            catch (OverflowException oEx)
            {
                Console.WriteLine(oEx.Message);
                result = -1;
            }
            finally
            {
                if (result == System.Double.NaN)
                    result = -1;
            }

            return result;


            // see also: Console.Read Method, Console.ReadLine Method, 
        }

        static void Unchecked()
        {
            int myVal = 0;
            unchecked
            {
                myVal = int.MaxValue + 1;
            }
            myVal = unchecked(int.MaxValue + 1);
            Console.WriteLine("myVal value is " + myVal.ToString());
        }

        static void CharPoChar()
        {
            string m1 = "\nType a string of text then press Enter. " +
                "Type '+' anywhere in the text to quit:\n";

            string m2 = "Character '{0}' is hexadecimal 0x{1:x4}.";
            string m3 = "Character     is hexadecimal 0x{0:x4}.";
            char ch;
            int x;
            //
            Console.WriteLine(m1);
            do
            {
                x = Console.Read();
                try
                {
                    ch = Convert.ToChar(x);
                    if (Char.IsWhiteSpace(ch))
                    {
                        Console.WriteLine(m3, x);
                        if (ch == 0x0a)
                            Console.WriteLine(m1);
                    }
                    else
                        Console.WriteLine(m2, ch, x);
                }
                catch (OverflowException e)
                {
                    Console.WriteLine("{0} Value read = {1}.", e.Message, x);
                    ch = Char.MinValue;
                    Console.WriteLine(m1);
                }
            } while (ch != '+');
        }

        static void ifStatements(int day)
        {
            string dayName;


            if (day == 0)
                dayName = "Sunday";
            else if (day == 1)
                dayName = "Monday";
            else if (day == 2)
                dayName = "Tuesday";
            else if (day == 3)
                dayName = "Wednesday";
            else if (day == 4)
                dayName = "Thursday";
            else if (day == 5)
                dayName = "Friday";
            else if (day == 6)
                dayName = "Saturday";
            else
                dayName = "unknown";

            Console.WriteLine("Name of the day is {0}", dayName);

            bool bval1, bval2, bval3, bval4, bval5, bval6;  //true, false

            bval1 = (day == 0);
            bval2 = (day > 0);
            bval3 = (day <= 10);
            bval4 = (day != 3);

            bval5 = bval1 && bval2;
            bval6 = (!bval1 && bval2) || (bval1 && bval3);

            if (bval1 || bval2 || bval3)
                Console.WriteLine("One of the first three bools is true");

            // bitwise operacije, ne mesati ih sa logickim...
            int a = 0xFFFF;
            int b = 0xF0F0;
            Console.WriteLine("Bitwise operations: {0:X}, {1:X}, {2:X}", a & b, a | b, a ^ b);
            // see also: C# Operators
        }

        static void switchStatements(byte digit)
        {
            switch (digit)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 9:
                    Console.WriteLine("Digit is odd");
                    break;
                case 2:
                case 4:
                case 6:
                case 8:
                    Console.WriteLine("Digit is even");
                    break;
                default:
                    Console.WriteLine("Unknown digit");
                    break;
            }
        }
    }
}
