﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static System.Math;


namespace _P1_01_proceduralno
{
    partial class Program
    {
        static double myMethod(int param1, double param2, string param3)
        {
            Console.WriteLine("Params of myMethod are: {0}, {1} and \"{2}\"", param1, param2, param3);

            return param1 + param2;
        }

        // overloaded myMethod - mora da se razlikuje bar u jednom delu zaglavlja
        static double myMethod(int param1, double param2)
        {
            Console.WriteLine("Params of myMethod are: {0} and {1}", param1, param2);

            return param1 + param2;
        }

        static double myOtherMethod(int param1 = 2, double param2 = 0.1, string param3 = "hello world")
        {
            Console.WriteLine("Params of myMethod1 are: {0}, {1} and \"{2}\"", param1, param2, param3);

            return param1 + param2;
        }


        private static void Loops()
        {
            int s = 0;
            for (int i = 0; i < 10; i++)
                s += i;

            // while ciklus -   max. stepen dvojke manji ili jednak n
            int maxPow2 = 1, n = 1234567;
            while (maxPow2 <= n)
                maxPow2 *= 2;
            maxPow2 /= 2;

            // do-while ciklus
            maxPow2 = 1;
            do
            {
                maxPow2 *= 2;
            }
            while (maxPow2 <= n);
        }

        static void mathFunctions()
        {
            // Umesto Math.PI, Math.Log, Math.Cos, pisemo samo Pi, Log, Cos (using static System.Math)

            double var = PI * 2;
            var = Log(var);
            var = Cos(var);
            
            Console.WriteLine("Final value of variable var is : {0}", var);
            //throw new NotImplementedException();
        }

    }
}
