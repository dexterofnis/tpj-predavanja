﻿using System;
using System.IO;


using static System.Console;

namespace _P1_01_proceduralno
{
    partial class Program
    {

        static void Main(string[] args)
        {

            // Prvo i osnovno!!!
            Console.WriteLine("Hello World");
            WriteLine("Hello World");   // koriscenjem using static System.Console; Prvi put u C# 6.0

            // Deklaracija promenljivih, kao CPP i Java
            int myInt = -1234, myInt1;
            var myString = "hey!";
            double trouble = 123.45;


            //myInt = myInt1 - 1; //- kompajlerska greska

            myInt = myInt + 1;
            myInt++; ++myInt; myInt--; --myInt;
            myInt = myInt - 1;
            myInt = myInt * 1;
            myInt = myInt / 1;
            myInt = myInt % int.MaxValue;
            

            Console.WriteLine(myInt);
            Console.WriteLine(myString);
            
            Ispis(myInt, trouble);

            // Procedure i funkcije (metode)
            // hide selection
            trouble = myMethod(myInt, trouble, "blabla");
            myMethod(myInt, trouble);


            myOtherMethod(100, 200, "DeXteR!");
            myOtherMethod(100, 200);
            myOtherMethod(100);
            myOtherMethod();

            myOtherMethod(param1: 1, param3: "DeXteR!");
            myOtherMethod(param3: "DeXteR[ity]!", param2: 1);
           
            // promeni ime promenljive i generisi metod refaktorizacijom koda...
            // comment (CTRL + K,C) i uncomment (CTRL + K,U) selection...
            // extract method

            mathFunctions();

            // if i switch naredba (isto kao u C++)
            int day = 1;
            ifStatements(day);

            switchStatements(4);

            // ciklusi (loops) (isto kao u C++)
            // extract method
            // debugiranje programa (watchies, breakpoints, ... )

            // for ciklus   -   zbir brojeva od 0 do 9
            Loops();

            // ucitavanje (komplikovano :) ) i exceptions...
            double result = Ucitavanje(ref myInt, ref trouble);
            Console.Write("Result is: ");
            Console.WriteLine(result);

            Unchecked();
                        
            CharPoChar();

        }

    }
}
