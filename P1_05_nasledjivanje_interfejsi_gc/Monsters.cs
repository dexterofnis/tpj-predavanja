﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_05_nasledjivanje_interfejsi_gc
{
    interface IMonster
    {
        // navodi se samo tip i zaglavlje metode, ne stoje modifieri
        // podrazumeva se da su metode public
        int NumberOfHeads();
    }

    interface IBird
    {
        bool CanFly();
        int NumberOfHeads();
    }

    interface IFire1
    {
        bool CanFire();
    }

    interface IFire2 : IFire1
    {
        int FireIntensity();
    }

    class Hydra : Mammal, IMonster
    {
        // metode iz interfejsa moraju da budu public
        public int NumberOfHeads() => 5;
    }


    class Dragon : Mammal, IMonster, IBird, IFire2 //,IFire1
    {

        // eksplicitno navodjenje interfejsa, ukoliko metoda postoji u vise od jednom interfejsu
        // ne stoji modifier, zato sto ova metoda ne moze da se pozove iz klase Dragon, vec samo
        // ako se ista posmatra kao implementacija interfejsa ( (myDragon as IMonster).NumberOfHeads() )
        int IMonster.NumberOfHeads() => 2;
        int IBird.NumberOfHeads() => 1;
        public bool CanFly() => true;
        public bool CanFire() => true;
        public int FireIntensity() => 10;
    }
}
