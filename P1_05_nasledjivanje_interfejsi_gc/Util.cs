﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_05_nasledjivanje_interfejsi_gc
{

    // pomocna (staticka) klasa u kojoj se definise extension metoda
    static class Util
    {

        // extension metoda za int
        public static int Negate(this int i)
        {
            return -i;
        }

        public static string BaseN(this int i, int N)
        {
            string res = "";
            int c;
            
            while (i != 0)
            {
                c = i % N;
                res = c.ToString() + res;
                i /= N;
            }
            
            return res;
        }
    }

}