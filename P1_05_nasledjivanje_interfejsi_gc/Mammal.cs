﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_05_nasledjivanje_interfejsi_gc
{

    class Mammal   
    {
        private string name = "Noname"; // nije vidljivo u nasledjenim klasama
        protected string species = "Zivuljka";  // vidljivo u nasledjenim klasama, ali ne izvan!!

        public string GetName() => name;    // umesto return name

        // mora eksplicitno da se navede, ukoliko postoji jos neki konstruktor!!
        public Mammal()
        { }

        public Mammal(string name)
        {
            this.name = name;
            //Console.WriteLine("Konstruktor za zivotinju : {0}...", name);
        }

        public void Breathe() => Console.WriteLine(name + " breathing...");

        public void SuckleYoung() =>  Console.WriteLine(name + " suckling...");

        public override string ToString() => name;

        // ne moze da se definise cista virtuelna metoda, ako klasa nije apstraktna
        public virtual void Talk() =>  Console.WriteLine(name + " talking...");
    }
}
