﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_05_nasledjivanje_interfejsi_gc
{
    class Zivotinja
    {
        private string name = "Noname";

        public string GetName() => name;

        public void SetName(string name)
        {
            this.name = name; 
        }

        public Zivotinja()
        { }

        public Zivotinja(string name)
        {
            this.name = name;
            //Console.WriteLine("Konstruktor za zivotinju : {0}...", name);
        }

        public void Breathe()
        {
            Console.WriteLine(name + " breathing...");
        }
    }

    abstract class Poultry : Zivotinja, IBird
    {
        // cista virtuelna (apstraktna) metoda
        public abstract bool CanFly();

        public Poultry()
            : base() { }

        public Poultry(string name)
            : base(name) { }

        public int NumberOfHeads() => 1;
    }

    class Chicken : Poultry
    {
        // posto nasledjuje apstraktnu klasu, klasa Chicken mora overrideuje CanFly
        public override bool CanFly() => false;

        public override string ToString() => "Kokoska " + GetName();

        public string WriteBothTwoStringMethods()
        {
            return this.ToString() + " | " + base.ToString();
        }
    }

    sealed class Duck : Poultry
    {
        public bool wild = true;

        public Duck(bool wild)
            :base("Duck")
        {
            this.wild = wild;
        }

        public override bool CanFly() => wild;
    }

}
