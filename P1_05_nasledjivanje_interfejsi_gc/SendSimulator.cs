﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_05_nasledjivanje_interfejsi_gc
{
    class SendSimulator : IDisposable
    {
        private string[] buffer = null;
        int position = 0;
        private bool disposed = false;

        public SendSimulator(int size)
        {
            buffer = new string[size];
        }

        public void Send(string data)
        {
            if (position < buffer.Length)
            {
                buffer[position++] = data;
            }
            else
            {
                Flush();
                buffer[position++] = data;
            }
        }

        private void Flush()
        {
            for (int i = 0; i < position; i++)
                Console.WriteLine(buffer[i]);
            position = 0;
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Flush();
                disposed = true;

                // disableuje poziv destruktora posto je objekat
                // upravo dispose-ovan. Mada, promenljiva disposed
                // u svakom slucaju sprecava da se Dispose pozove
                // vise od jednom
                GC.SuppressFinalize(this);
            }
        }

        ~SendSimulator()
        {
            Console.WriteLine("Destruktor za SendSimulator");
            Flush();
        }

    }
}
