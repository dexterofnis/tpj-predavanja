﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace _P1_05_nasledjivanje_interfejsi_gc
{
    class Program
    {
        static void Main(string[] args)
        {
            // nasledjivanje 
            Inheritance();

            // extension metode
            ExtensionMethods();
            
            // interfejsi
            Interfaces();

            // desktruktori, using naredbe, gc...
            DestructorVSUsing();
        }

        private static void Inheritance()
        {
            // moze samo sa klasama, ne i sa strukturama!!!
            Mammal myMammal = new Mammal("Pera");
            Horse myHorse = new Horse("Sharac");

            myHorse.SuckleYoung();      // poziva metodu za Horse
            myMammal.SuckleYoung();     // poziva metodu za Mammal
            
            Console.WriteLine();
            myMammal = myHorse;
            myMammal.SuckleYoung();             // poziva fju za Mammal
            myHorse.SuckleYoung();              // poziva fju za Horse
            (myHorse as Mammal).SuckleYoung();  // poziva fju za Mammal
            Console.WriteLine();
            

            myMammal.Talk();            // virtual-override : poziva fju za Horse, iako je promenljiva tipa Mammal
            

            Whale myWhale = new Whale("Moby-Dick");
            myMammal = myWhale;
            // myMammal.Talk();
            myWhale = myMammal as Whale;
            (myMammal as Whale).Swim();
            // myWhale.Talk(); - daje exception zato sto kitovi ne govore ;)
        }

        private static void ExtensionMethods()
        {
            int i = 123456789;

            Console.WriteLine(Util.BaseN(i, 2));
            
            Console.WriteLine(i.BaseN(2));
            Console.WriteLine(i.Negate());
        }


        private static void Interfaces()
        {
            Hydra myHydra = new Hydra();
            IMonster iMyHydra = myHydra;
            Console.WriteLine(iMyHydra.NumberOfHeads());
            Console.WriteLine(myHydra.NumberOfHeads());

            Dragon myDragon = new Dragon();
            
            // Pozivanje zajednicke metode eksplicitnim navodjenjem interfejsa
            Console.WriteLine("{0} .... {1}",
                (myDragon as IMonster).NumberOfHeads(),
                (myDragon as IBird).NumberOfHeads()
                );
            
            Console.WriteLine(myDragon.CanFire());
            Console.WriteLine((myDragon as IFire1).CanFire());

            // ne moze myDragon.NumberOfHeads(); 

            Chicken myChicken = new Chicken();
            Console.WriteLine(myChicken.CanFly());
            Console.WriteLine(myChicken.WriteBothTwoStringMethods());

            Zivotinja z = myChicken;
            Poultry myPoultry = myChicken;
            //myPoultry = new Poultry();
            myPoultry = new Chicken();
            Console.WriteLine(myPoultry.CanFly());  // poziva CanFly() za Poultry
        }

        private static void DestructorVSUsing()
        {
            // nakon sto se deo unutar using zavrsi, poziva se Dispose metod objekta
            // objekat mora da implementira IDisposable, tj. metod dispose

            using (SendSimulator ss = new SendSimulator(3))
            {
                ss.Send("Teorija");
                ss.Send("Programskih");
                ss.Send("Jezika");
                ss.Send("je");
                ss.Send("cool");
            }

            SendSimulator ss1 = new SendSimulator(2);
            ss1.Send("Teorija");
            ss1.Send("Programskih");
            ss1.Send("Jezika");
            Thread.Sleep(5000);
        }


    }
}
