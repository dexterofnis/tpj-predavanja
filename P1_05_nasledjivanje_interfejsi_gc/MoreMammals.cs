﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _P1_05_nasledjivanje_interfejsi_gc
{

    // ne postoji mogucnost specificiranja nacina nasledjivanja (private, protected, public)
    // sva nasledjivanja su public!!!

    class Horse : Mammal
    {
        public void Trot() => Console.WriteLine(GetName() + " trotting...");

        public Horse(string name)
            : base(name) // Poziv konstruktora za roditelja
        {
            //Console.WriteLine("Konstruktor za Horse...");
            species = "Horse";
        }

        // samo disableuje warning...
        new public void SuckleYoung()
        {
            Console.WriteLine("Horse " + GetName() + " suckling");
            //base.SuckleYoung();
        }
           
        // premoscuje - redefinise (overrideuje) fju za Mammal-a
        public override void Talk()
        {
            // base.Talk(); - poziv funkcije definisane u roditeljskoj klasi
            
            Console.WriteLine($"Horse {GetName()} nickering...");
        }

        public override string ToString()
        {
            return "Horse " + GetName();
        }
    }


    class Whale : Mammal
    {
        public Whale(string name)
            : base(name)
        {
            species = "Whale";
        }

        public override void Talk()
        {
            throw new NotImplementedException();
        }

        public void Swim() => Console.WriteLine(GetName() + " swimming...");
    }


}
