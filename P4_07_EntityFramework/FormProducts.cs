﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data.Entity;

namespace _P4_07_EntityFramework
{
    public partial class FormProducts : Form
    {
        public FormProducts()
        {
            InitializeComponent();
        }

        NorthwindEntities nwContext = Program.nwContext;


        void SubmitChanges()
        {
            try
            {
                nwContext.SaveChanges();
                
                // bez ovoga, kad se doda novi red ne updateuje primary key (ostane 0). 
                // Shvatio nakon ~1h debuggiranja!
                dgProducts.Refresh();
            }
            catch (Exception ex)
            {
                // Javila se neka druga greska
                MessageBox.Show(ex.InnerException.Message, "An error has occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            MessageBox.Show("Update successfully completed!", "Update complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void FormProducts_Load(object sender, EventArgs e)
        {
            supplierBindingSource.DataSource = nwContext.Suppliers.ToList();
            categoryBindingSource.DataSource = nwContext.Categories.ToList();

            nwContext.Products.Load();  // kao ToList() samo se lista odbacuje!!
            productBindingSource.DataSource = nwContext.Products.Local.ToBindingList();

            //dgProducts.DataSource = nwContext.Products.Local.ToBindingList();

            // Moze i ovako iz koda... 
            //supplierDataGridViewComboBoxColumn.DataSource = nwContext.Suppliers.ToList();
            //supplierDataGridViewComboBoxColumn.DataPropertyName = "SupplierID";
            //supplierDataGridViewComboBoxColumn.DisplayMember = "CompanyName";
            //supplierDataGridViewComboBoxColumn.ValueMember = "SupplierID";

            //categoryIDDataGridViewComboBoxColumn.DataSource = nwContext.Categories.ToList();
            //categoryIDDataGridViewComboBoxColumn.DataPropertyName = "CategoryID";
            //categoryIDDataGridViewComboBoxColumn.DisplayMember = "CategoryName";
            //categoryIDDataGridViewComboBoxColumn.ValueMember = "CategoryID";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SubmitChanges();
        }

        private void dgProducts_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }
    }
}
