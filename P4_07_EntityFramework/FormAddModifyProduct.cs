﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace _P4_07_EntityFramework
{
    public partial class FormAddModifyProduct : Form
    {
        public Product SelectedProduct
        { get; set; }

        NorthwindEntities nwContext = Program.nwContext;

        public FormAddModifyProduct()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmAddModify_Load(object sender, EventArgs e)
        {
            txtProductName.DataBindings.Add("Text", SelectedProduct, "ProductName", false, DataSourceUpdateMode.OnPropertyChanged, "");

            cmbCategory.DataSource = nwContext.Categories.ToList();
            cmbCategory.DataBindings.Add("SelectedItem", SelectedProduct, "Category");
            //SelectedProduct.Category = cmbCategory.SelectedItem as Category;

            txtQuantityPerUnit.DataBindings.Add("Text", SelectedProduct, "QuantityPerUnit", true, DataSourceUpdateMode.OnValidation, "");
            txtReorderLevel.DataBindings.Add("Text", SelectedProduct, "ReorderLevel", true, DataSourceUpdateMode.OnValidation, "");
            txtUnitPrice.DataBindings.Add("Text", SelectedProduct, "UnitPrice", true, DataSourceUpdateMode.OnValidation, "");
            txtUnitsInStock.DataBindings.Add("Text", SelectedProduct, "UnitsInStock", true, DataSourceUpdateMode.OnValidation, "");
            txtUnitsOnOrder.DataBindings.Add("Text", SelectedProduct, "UnitsOnOrder", true, DataSourceUpdateMode.OnValidation, "");
            cbDiscontinued.DataBindings.Add("Checked", SelectedProduct, "Discontinued", false, DataSourceUpdateMode.OnValidation, "");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void txt_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (sender as TextBox);
            int value;

            if (!int.TryParse(txt.Text, out value) && (txt.Text != ""))
            {
                e.Cancel = true;
                errorProvider1.SetError(txt, "Value must be a valid number");
            }
        }

        private void txt_Validated(object sender, EventArgs e)
        {
            TextBox txt = (sender as TextBox);
            errorProvider1.SetError(txt, "");
        }

        private void txtProductName_Validating(object sender, CancelEventArgs e)
        {
            TextBox txt = (sender as TextBox);
            if (txt.Text == "")
            {
                e.Cancel = true;
                errorProvider1.SetError(txt, "This value is required");
            }
        }
    }
}
