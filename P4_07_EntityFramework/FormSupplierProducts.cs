﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



namespace _P4_07_EntityFramework
{
    public partial class FormSupplierProducts : Form
    {


        public FormSupplierProducts()
        {

            InitializeComponent();
        }

        NorthwindEntities nwContext = Program.nwContext;

        private void FormSupplierProducts_Load(object sender, EventArgs e)
        {
            supplierBindingSource.DataSource = nwContext.Suppliers.ToList();
            categoryBindingSource.DataSource = nwContext.Categories.ToList();

            UpdateProductDataGrid();
        }

        private void UpdateProductDataGrid()
        {
            Supplier supplier = (cmbSuppliers.SelectedItem) as Supplier;
            

            // Zbog redosleda dispose-ovanja komponenti. Ako se BindingSource dispose-uje pre ComboBox-a, onda je supplier==null
            if (supplier == null) return;

            // LINQ to Entities... LINQ moze da se primenjuje na entitete (EntityCollection<T>)
            productBindingSource.DataSource = from prod in supplier.Products
                                              where !prod.Discontinued || !cbHideDisc.Checked
                                              select prod;
            
            // http://msdn.microsoft.com/en-us/library/system.data.datacolumn.expression.aspx
            // productBindingSource.Filter = "ProductName = 'Dexter'";
        }

        private void cmbSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateProductDataGrid();          
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SubmitChanges();
        }

        void SubmitChanges()
        {
            try
            {
                nwContext.SaveChanges();
            }
            catch (Exception ex)
            {
                // Javila se neka druga greska
                MessageBox.Show(ex.InnerException.Message, "An error has occured");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Supplier supplier = cmbSuppliers.SelectedItem as Supplier;

            // obrisi sve Product-e koji odgovaraju selektovanim redovima
            foreach (DataGridViewRow row in dgProducts.SelectedRows)
                nwContext.Products.Remove(row.DataBoundItem as Product);
                //supplier.Products.Remove(row.DataBoundItem as Product);

            SubmitChanges();

            UpdateProductDataGrid();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Supplier supplier = cmbSuppliers.SelectedItem as Supplier;

            // Kreiraj novi objekat
            Product newprod = nwContext.Products.Create();         

            // Otvori formu za Add/Modify
            FormAddModifyProduct frm = new FormAddModifyProduct();
            frm.SelectedProduct = newprod;
            
            if (frm.ShowDialog() == DialogResult.OK)
            {
                // Povezi ga sa odgovarajucim Supplierom i ubaci promene u bazu
                //newprod.Supplier = supplier;
                supplier.Products.Add(newprod);
                SubmitChanges();
            }
            else
            {
                // Izbacivanje objekta iz kolekcija
                supplier.Products.Remove(newprod);
            }

            UpdateProductDataGrid();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Supplier supplier = cmbSuppliers.SelectedItem as Supplier;

            // Otvori formu za editovanje selektovanog suppliera
            FormAddModifyProduct frm = new FormAddModifyProduct();
            // DataBoundItem je property koji vraca Item (polje) vezan za odredjeni red u DataGridView
            frm.SelectedProduct = dgProducts.SelectedRows[0].DataBoundItem as Product;

            // Ovo je polje na poziciji (2,3) u gridu : dgProducts[3,2].Value

          
            if (frm.ShowDialog() == DialogResult.OK)
            {
                // Ako korisnik potvrdi izmene, one se prosledjuju u bazu
                SubmitChanges();
            }
            else
            {
                // U suprotnom, podaci se ponovo ucitaju iz baze

                nwContext.Entry(frm.SelectedProduct).Reload();
            }

            UpdateProductDataGrid();
        }

        private void cbHideDisc_CheckedChanged(object sender, EventArgs e)
        {
            UpdateProductDataGrid();
        }
       
    }
}
