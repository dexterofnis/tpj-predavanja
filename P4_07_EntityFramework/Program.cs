﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace _P4_07_EntityFramework
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        // glavni objekat koji predstavlja bazu podataka
        public static NorthwindEntities nwContext;
        
        [STAThread]
        static void Main()
        {
            nwContext = new NorthwindEntities();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (MessageBox.Show("Show Products (Yes) or SupplierProducts (No) form?", "Select form", MessageBoxButtons.YesNo) == DialogResult.Yes)
                Application.Run(new FormProducts());
            else
                Application.Run(new FormSupplierProducts());

        }
    }
}
