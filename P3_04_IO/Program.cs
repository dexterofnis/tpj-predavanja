﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace _P3_04_IO
{
    partial class Program
    {
        static void Main(string[] args)
        {
            WriteToFile();
            
            CopyFile("test.txt", "test1.txt");
            
            // Klase StreamReader i StreamWriter nasledjuju apstraktne klase TextReader i TextWriter
            // koje sluze za citanje odnosno upis teksta. Poseduju metode Read() i ReadLine(), odnosno Write() i WriteLine()
            GetCDirs();


            // redirekcija standardnog ulaza i izlaza
            RedirectStandardIO();
            

            // BinaryReader i BinaryWriter
            BinaryRandomNumbers();
       
            // Random access to the file

            // po defaultu fajl se otvara tako da je dozvoljeno i citanje i upis
            // samo za citanje : FileStream fs = new FileStream("test1.txt", FileMode.Open, FileAccess.Read);
            FileRandomAccess();

            // MemoryStream
            MemoryStreamIO();
            

            StreamReaderBufferovanje();

            // StringReader i StringWriter slicni kao StreamReader i StreamWriter
                                    
            StariDobriScanf();

            // File i FileSystemWatcher klasa

            // see also : help fajlovi za sve pomenute klase
        }



    }
}
