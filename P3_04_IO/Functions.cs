﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Runtime.InteropServices;


namespace _P3_04_IO
{
    partial class Program
    {
        private static void WriteToFile()
        {
            // Stream je apstraktna klasa. Nasledjuju je FileStream, MemoryStream, itd.
            
            FileStream fout = null;
            try
            {
                // kreira novi objekat i otvara fajl
                fout = new FileStream("test.txt", FileMode.Create);
                // upisuje niz od 6 bajtova
                fout.Write(new byte[] { 1, 2, 3, 4, 5, 6 }, 0, 6);
                
                // Upisuje slova od 'A' do 'Z'
                for (char c = 'A'; c <= 'Z'; c++)
                    fout.WriteByte((byte)c);
            }
            catch (IOException exc)
            {
                Console.WriteLine("I/O Error:\n" + exc.Message);
            }
            finally
            {
                if (fout != null) fout.Close();
            }
        }

        private static void CopyFile(string infile, string outfile)
        {
            int i;
            FileStream fin = null;
            FileStream fout = null;
            try
            {
                // Otvori fajlove
                fin = new FileStream(infile, FileMode.Open, FileAccess.Read);
                fout = new FileStream(outfile, FileMode.Create, FileAccess.Write);

                // Ucitaj bajt iz jednog fajla, i upisi u drugi
                do
                {
                    i = fin.ReadByte();
                    if (i != -1)
                        fout.WriteByte((byte)i);
                } while (i != -1);

            }
            catch (IOException exc)
            {
                Console.WriteLine("I/O Error:\n" + exc.Message);
            }
            finally
            {
                fin?.Close();
                fout?.Close();
            }
        }

        private static void GetCDirs()
        {
            // Lista direktorijuma u c:\
            DirectoryInfo cDirInfo = new DirectoryInfo(@"c:\");
            DirectoryInfo[] cDirs = cDirInfo.GetDirectories();
            
            // Upisi u fajl
            // Klasa StreamWriter sluzi za upis tekstualnih podataka u Stream/Fajl
            using (StreamWriter sw = new StreamWriter("CDriveDirs.txt"))
            // StreamWriter sw1 = new StreamWriter(new FileStream("CDriveDirs.txt", FileMode.Create));
            {
                foreach (DirectoryInfo dir in cDirs)
                    sw.WriteLine(dir.Name);
            }

            // Ucitaj iz fajla i upisi na konzolu
            string line = "";
            using (StreamReader sr = new StreamReader("CDriveDirs.txt"))
            {
                while ((line = sr.ReadLine()) != null)
                    Console.WriteLine(line);
            }
        }

        private static void RedirectStandardIO()
        {
            StreamWriter log_out = null;

            try
            {
                log_out = new StreamWriter("logfile.txt");

                // Redirect standard out to logfile.txt.
                Console.SetOut(log_out);
                Console.WriteLine("This is the start of the log file.");
                for (int i = 0; i < 10; i++)
                    Console.WriteLine(i);
                Console.WriteLine("This is the end of the log file.");
            }
            catch (IOException exc)
            {
                Console.WriteLine("I/O Error\n" + exc.Message);
            }
            finally
            {
                if (log_out != null)
                    log_out.Close();
            }

            // Restore default standard output
            StreamWriter standardOutput = new StreamWriter(Console.OpenStandardOutput());
            standardOutput.AutoFlush = true;
            Console.SetOut(standardOutput);

            // slicno radi SetIn metoda klase Console
        }

        private static void BinaryRandomNumbers()
        {
            Random rnd = new Random();
            int totalNumbers = 10;

            using (BinaryWriter dataOut = new BinaryWriter(new FileStream("rndnumbers.dat", FileMode.Create)))
            {
                try
                {
                    for (int i = 0; i < totalNumbers; i++)
                        dataOut.Write(rnd.Next());
                    for (int i = 0; i < totalNumbers; i++)
                        dataOut.Write(rnd.NextDouble());
                }
                catch (IOException ex)
                {
                    Console.WriteLine("Error: {0}", ex.Message);
                }
            }


            using (BinaryReader dataIn = new BinaryReader(new FileStream("rndnumbers.dat", FileMode.Open)))
            {
                try
                {
                    for (int i = 0; i < totalNumbers; i++)
                        Console.WriteLine(dataIn.ReadInt32());
                    for (int i = 0; i < totalNumbers; i++)
                        Console.WriteLine(dataIn.ReadDouble());
                }
                catch (IOException ex)
                {
                    Console.WriteLine("Error: {0}", ex.Message);
                }
            }
        }

        private static void FileRandomAccess()
        {
            using (FileStream fs = new FileStream("test1.txt", FileMode.Open))
            {
                Random rnd = new Random();
                
                for (int i = 0; i < 10; i++)
                {
                    int pos = rnd.Next((int)fs.Length - 1);
                    // pozicija se indeksira od 0
                    fs.Seek(pos, SeekOrigin.Begin);
                    
                    char chr = (char)fs.ReadByte();
                    Console.WriteLine($"Character at the position {pos} is : {chr}");
                }

                fs.Seek(rnd.Next((int)fs.Length - 1), SeekOrigin.Begin);
                fs.WriteByte((byte)'X');

                // prosiri fajl za 3 bajta i u 3. bajtu upisi vrednost 'X'
                fs.Seek(2, SeekOrigin.End);
                fs.WriteByte((byte)'X');
            }
        }

        private static void MemoryStreamIO()
        {
            byte[] buffer = new byte[100];
            MemoryStream ms = new MemoryStream(buffer);

            BinaryWriter bw = new BinaryWriter(ms);
            BinaryReader br = new BinaryReader(ms);
            bw.Write(10);
            bw.Write(1.1);

            ms.Seek(0, SeekOrigin.Begin);

            int readint = br.ReadInt32();
            double readdouble = br.ReadDouble();
            Console.WriteLine($"Int: {readint}   Double: {readdouble}");
            
            ms.Seek(0, SeekOrigin.Begin);

            StreamWriter sw = new StreamWriter(ms);
            StreamReader sr = new StreamReader(ms);

            sw.AutoFlush = true;

            sw.WriteLine("Hello world!");
            sw.WriteLine("from text");
            // bw.Write((int)1);

            ms.Seek(0, SeekOrigin.Begin);

            string readstring = sr.ReadLine();
            string readstring1 = sr.ReadLine();
            Console.WriteLine($"{readstring} {readstring1}");
        }

        private static void StreamReaderBufferovanje()
        {
            // StreamReader koristi interni buffer podataka (velicine do 1024 karaktera). Prilikom poziva Read ili ReadLine metode, 
            // iz polaznog streama se ucita veca kolicina podataka i bufferuje. Zbog toga nije zgodno da vise od jednog StreamReadera
            // bude povezano na isti stream. Ista situacija je i sa StreamWriter klasom.

            // Ovo nije slucaj kod BinaryReader/BinaryWriter klase

            MemoryStream ms = new MemoryStream(new byte[100]);
            StreamWriter sw = new StreamWriter(ms);
            // sw.AutoFlush = true;

            sw.WriteLine("First line");
            sw.WriteLine("Second line");
            // Ukoliko se ovde ne pozove Flush, nista se ne upisuje u stream
            sw.Flush();


            ms.Seek(0, SeekOrigin.Begin);

            StreamReader sr = new StreamReader(ms);
            // Nakon ovog poziva, celokupan sadrzaj ms-a se prebacuje u interni buffer objekta sr. Zbog toga novonapravljeni StreamReader
            // ne ucitava drugi string.
            string str1 = sr.ReadLine();
            string str2 = (new StreamReader(ms)).ReadLine();
            
            Console.WriteLine($"Stringovi: {str1}, {str2}");
        }

        [DllImport("msvcrt.dll",
            CharSet = CharSet.Ansi,
            CallingConvention = CallingConvention.Cdecl)]
        public static extern int scanf(string format, ref int s1);

        [DllImport("msvcrt.dll",
            CharSet = CharSet.Ansi,
            CallingConvention = CallingConvention.Cdecl)]
        public static extern int scanf(string format, ref double s1);


        private static void StariDobriScanf()
        {
            int i = 0;
            double xx = 0;
            scanf("%d", ref i);
            scanf("%lf", ref xx);
            Console.WriteLine($"{i} {xx}");
        }


    }
}
